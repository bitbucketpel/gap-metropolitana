<?php include_once 'header.php'; ?>

<section class="header">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1>DESARROLLOS EN VENTA</h1>
            </div>
        </div>
    </div>
</section>

<section class="espacios">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h2>ESPACIO <span>MELLADO</span></h2>
                <ul>
                    <li>
                        <span class="icon-pin"></span>
                        Arteaga 33, Col. Guerrero, Del. Cuauhtémoc, CDMX
                    </li>
                    <li>
                        <span class="icon-building"></span>
                        Planta Baja y 5 niveles
                    </li>
                    <li>
                        <span class="icon-bodegas"></span>
                        Bodegas
                    </li>
                    <li>
                        <span class="icon-garage"></span>
                        Cajón de Estacionamiento
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="image-view">
                    <div class="info">
                        <div class="text">
                            Aceptamos Créditos <strong>INFONAVIT & BANCARIO</strong>
                        </div>
                        <div class="cta">
                            <p>Precios desde <br><span>$666,703.00</span></p>
                        </div>
                    </div>
                    <img src="images/preview-imagen-desarrollo.png" class="img-fluid" alt="">
                </div>
                <div id="desarrollo-carousel" class="desarrollo-carousel owl-carousel">
                    <div class="item-carousel">
                        <img src="images/control-acceso.png" class="img-fluid" alt="...">
                    </div>
                    <div class="item-carousel">
                        <img src="images/areas-comunes.png" class="img-fluid" alt="...">
                    </div>
                    <div class="item-carousel">
                        <img src="images/estacionamiento.png" class="img-fluid" alt="...">
                    </div>
                    <div class="item-carousel">
                        <img src="images/mellado.png" class="img-fluid" alt="...">
                    </div>
                    <div class="item-carousel">
                        <img src="images/mellado.png" class="img-fluid" alt="...">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-title">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <h2>PROTOTIPOS</h2>
            </div>
        </div>
    </div>
</section>

<section class="prototipos">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-5 col-lg-5 col-md-7">
                <div class="list-items">
                    <div class="item-prototipo">
                        <h3>NM-01</h3>
                        <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                        <p><strong>s/balcón(5) * c/balcón(0)</strong></p>
                    </div>
                    <div class="item-prototipo">
                        <h3>NM-01</h3>
                        <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                        <p><strong>s/balcón(5) * c/balcón(0)</strong></p>
                    </div>
                    <div class="item-prototipo">
                        <h3>NM-01</h3>
                        <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                        <p><strong>s/balcón(5) * c/balcón(0)</strong></p>
                    </div>
                    <div class="item-prototipo">
                        <h3>NM-01</h3>
                        <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                        <p><strong>s/balcón(5) * c/balcón(0)</strong></p>
                    </div>
                    <div class="item-prototipo">
                        <h3>NM-01</h3>
                        <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                        <p><strong>s/balcón(5) * c/balcón(0)</strong></p>
                    </div>
                    <div class="item-prototipo">
                        <h3>NM-01</h3>
                        <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                        <p><strong>s/balcón(5) * c/balcón(0)</strong></p>
                    </div>
                    <div class="item-prototipo">
                        <h3>NM-01</h3>
                        <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                        <p><strong>s/balcón(5) * c/balcón(0)</strong></p>
                    </div>
                    <div class="item-prototipo">
                        <h3>NM-01</h3>
                        <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                        <p><strong>s/balcón(5) * c/balcón(0)</strong></p>
                    </div>
                    <div class="item-prototipo">
                        <h3>NM-01</h3>
                        <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                        <p><strong>s/balcón(5) * c/balcón(0)</strong></p>
                    </div>
                    
                </div>
                <div class="total-deptos">
                    <h3>Total de departamentos: 195</h3>
                </div>
                <div class="distribucion">
                    <h3>Distribución</h3>
                    <ul>
                        <li><span class="icon-room"></span>Recamaras (2)</li>
                        <li><span class="icon-kitchen"></span>Cocina</li>
                        <li><span class="icon-toilet"></span>Baño</li>
                        <li><span class="icon-building"></span>No. Deptos (5)</li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-7">
                <div class="info-prototipo">
                    <h3>NM-01</h3>
                    <p><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                    <img src="images/prototipos-1.png" class="img-fluid image-prototipo" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <span class="icon-pin"></span>
                <h2>CONTACTO</h2>
                <p>Visita Nuestras oficinas de ventas</p>
            </div>
            <div class="col-xl-6">
                <a href="#" class="btn">CONTACTAR</a>
            </div>
        </div>
    </div>
</section>

<?php include_once 'footer.php'; ?>