<?php include_once('header.php'); ?>

<section class="header quienes-somos">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1>GAP METROPOLITANA</h1>
            </div>
        </div>
    </div>
</section>

<section class="aviso-privacidad">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h2>AVISO DE <span>PRIVACIDAD</span></h2>
                <p>En cumplimiento con lo establecido por la Ley Federal de Protección de Datos Personales en Posesión de Particulares publicada en el Diario Oficial de la Federación el día 5 de Julio de 2010 (la “Ley”), se extiende el presente Aviso de Privacidad.</p>
                <h3>. Responsable del Tratamiento de Datos Personales</h3>
                <p>GAP METROPOLITANA, S.A. DE C.V. y OPERADORA DF20, S.A. DE C.V. en lo sucesivo (“La empresa”) cuyas oficinas corporativas se encuentran ubicadas en Avenida Paseo de la Reforma Número 403 Despacho 601, Cuauhtémoc, Ciudad de México, C.P. 06500, por medio del presente documento hace de su conocimiento que sus datos personales serán tratados estrictamente para los fines que más adelante se señalan.</p>
                <h3>II. Datos Personales</h3>
                <p>Los datos personales que serán tratados por “La empresa”, consisten en información personal del titular, la cual puede comprender:</p>
                <p>Nombre completo, lugar y fecha de nacimiento, edad, sexo, estado civil, Registro Federal de Contribuyentes (RFC), firma autógrafa, domicilio, teléfono fijo y móvil, correo electrónico personal y laboral, experiencia profesional, referencias personales y profesionales, situación socio económica, cuenta bancaria para depósitos de nómina, número de seguro social, historial académico, empleo actual; información patrimonial y financiera relacionada con bienes y derechos, cargas u obligaciones susceptibles de valor económico, como información fiscal, historial crediticio, verificar si usted se encuentra o no en el buró de crédito, ingresos y egresos, cuentas bancarias, seguros, afores, fianzas; asimismo los datos personales de su cónyuge (en su caso) y/o fiador (en su caso), siendo éstos los estrictamente indispensables para establecer una relación jurídica contractual con nosotros</p>
                <p>Entre otra estrictamente indispensable para los fines que posteriormente se mencionan, dentro de la cual se encuentran datos personales sensibles; se consideran datos personales sensibles aquellos que afecten a la esfera más íntima de su titular, o cuya utilización indebida pueda dar origen a discriminación o conlleve un riesgo grave para éste. En particular, se consideran sensibles aquellos datos personales que puedan revelar aspectos como origen racial o étnico, estado de salud presente y futuro, información genética, creencias religiosas, filosóficas y morales, afiliación sindical, opiniones políticas, preferencias sexuales.</p>
                <p>En caso de no contar con sus datos personales no estaríamos en posibilidad de llevar a cabo los fines para los cuales se requieren, por lo que “La empresa” no tendría ningún tipo de responsabilidad derivado de ello.</p>
                <h3>III. Finalidades del Tratamiento de Datos Personales</h3>
                <p>“La empresa” recabará sus datos personales sin fines de divulgación o utilización comercial, y será únicamente para los siguientes fines:</p>
                <ul>
                    <li>Evaluación como posible candidato para ocupar algún puesto vacante.</li>
                    <li>En su caso la elaboración de Contrato Laboral.</li>
                    <li>El cumplimiento de obligaciones legales, fiscales, laborales, seguridad social, entre otras que deriven de la relación laboral y comercial.</li>
                    <li>Potenciales procesos administrativos o judiciales que involucren al titular de los datos personales.</li>
                    <li>La integración de un expediente que permita la tramitación de crédito para la adquisición de vivienda o unidad privativa en alguno de nuestros desarrollos habitacionales, así como la propia adquisición de una vivienda o unidad privativa en alguno de nuestros desarrollos habitacionales.</li>
                </ul>
                <p>Los datos personales serán guardados bajo la más estricta confidencialidad y no se les podrá dar un uso distinto a los antes mencionados, salvo que “La empresa” realice un cambio en este Aviso de Privacidad, el cual será notificado a través de los medios mencionados por la “La empresa”.</p>
                <p>Una vez que se cumpla la finalidad del tratamiento de datos personales, éstos serán bloqueados con el único propósito de determinar posibles responsabilidades en relación con su tratamiento, hasta el plazo de prescripción legal o contractual de éstas. Durante dicho periodo, los datos personales no podrán ser objeto de tratamiento y transcurrido éste, se procederá a su cancelación en la base de datos que corresponde.</p>
                <p>No obstante lo anterior, los datos personales de los candidatos que no hayan resultado viables para ocupar la vacante para la cual aplicaron, serán eliminados inmediatamente y no serán conservados en ninguna base de datos.</p>
                <p>Los expedientes de los clientes que no les sea autorizado su crédito y/o de aquellos que no concreten la compra, su información será eliminada de nuestras bases de datos.</p>
                <h3>IV. Recolección de los Datos Personales</h3>
                <p>Para la recolección de datos personales, seguimos todos los principios que marca la Ley como la licitud, calidad, consentimiento, información, finalidad, lealtad, proporcionalidad y responsabilidad.</p>
                <p>Los datos personales serán recabados directamente del titular de forma personal o a través de otros medios ópticos, sonoros, visuales, o por cualquier otra tecnología legalmente permitida, ya sea mediante solicitudes, currículos, entrevistas, estudios socio-económicos, evaluaciones médicas, y psicométricas, en nuestra página de internet o servicios en línea, entre otras.</p>
                <p>La información que recibimos y almacenamos de nuestras páginas de internet cuando usted interactúa en nuestros sitios web, es principalmente información técnica como su dirección de protocolo de internet, su sistema operativo en la computadora y su tipo de navegador, la dirección de un sitio web de referencia, en su caso, y la ruta que siga durante su recorrido por nuestra página web. Usamos "cookies" para reconocerlo cuando utilice o regrese a nuestros sitios. Una "cookie" es un pequeño archivo de texto que un sitio web o un correo electrónico puede salvar en su navegador y almacenarlo en su disco duro. Usted puede elegir navegar en nuestros sitios web sin aceptar las cookies. Las cookies nos permiten reconocerlo de página en página, y respaldan sus operaciones con nosotros. Sin las cookies habilitadas, usted podrá navegar en nuestros sitios web, pero es posible que no pueda utilizar algunas características del sitio web. Se recaba esta información para permitir que los sitios web operen correctamente, evaluar el uso del sitio web y apoyar las campañas promocionales.</p>
                <p>Además, podremos obtener información del titular de otras fuentes permitidas por la ley, tales como directorios telefónicos o laborales, bolsas de trabajo, referencias de otras empresas o particulares, bases de datos públicas de cualquier entidad o dependencia pública o privada, entre otras.</p>
                <h3>V. Opciones y medios para limitar el uso o divulgación de los datos.</h3>
                <p>En “La empresa” se cuenta con las medidas de seguridad, administrativas, técnicas y físicas necesarias y suficientes para proteger sus datos personales contra daño, pérdida, alteración, destrucción, uso, acceso o tratamiento no autorizado.</p>
                <p>Los datos personales son salvaguardados en bases de datos y equipos de cómputo que cuentan con la seguridad necesaria para prevenir fugas de información. Controles de acceso físico y lógico, controles ambientales, sistemas de protección anti intrusos (Firewall), herramientas de protección antivirus y filtrado web son algunas de las herramientas utilizadas para mantener la seguridad de los datos en los sistemas de información de “La empresa”.</p>
                <p>Las herramientas de seguridad informática mencionadas en el párrafo anterior están apoyadas por una política interna de seguridad de la información que explica a los empleados las consideraciones de seguridad que deben tomar en cuenta al utilizar un sistema informático y es reforzada constantemente.</p>
                <p>Los datos personales que “La empresa” solicita pueden ser compartidos dentro del país con las siguientes Instituciones: INFONAVIT, FOVISSSTE, Sociedad Hipotecaria Federal, Instituciones Financieras Bancarias, Sociedades Financieras de Objeto Limitado (SOFOLES), Sociedades Financieras de Objeto Múltiple (SOFOMES), o cualquier otra Institución que otorgue o gestione financiamiento para la adquisición de bienes inmuebles; igualmente al Buró de Crédito. Lo anterior para los fines de tramitación y otorgamiento de crédito con el propósito de que adquiera una vivienda o unidad privativa con nosotros.</p>
                <p>Por ello, se hacen de su conocimiento las opciones que usted tiene para solicitar la limitación del uso o divulgación de sus datos personales, materia de este Aviso:</p>

                <ul>
                    <li>Por escrito enviado al Encargado del Tratamiento de Datos Personales (en el domicilio señalado en el punto VI siguiente); o vía correo electrónico dirigido a la dirección jalarrazolo@gapmet.com.mx o agarcia@gapmet.com.mx</li>
                    <li>Vía telefónica con el Encargado del Tratamiento de Datos Personales al teléfono 52022847.</li>
                </ul>

                <h3>VI. Encargado del Tratamiento de los Datos Personales</h3>

                <p>El encargado del Tratamiento de los Datos Personales que nos proporcionan nuestros clientes cuando evaluamos la posibilidad de su crédito en “La empresa” es el C. Alfonso García Escobedo, con domicilio en Avenida Paseo de la Reforma Número 403 Despacho 601, Cuauhtémoc, Ciudad de México, C.P. 06500, horarios de atención de 9:00 a 18:00 horas de lunes a viernes, correo electrónico agarcia@gapmet.com.mx teléfono 52022847.</p>
                <p>El Encargado del Tratamiento de Datos Personales de los Empleados, de los proveedores y de cualquier otra persona que nos brinde información personal en “La empresa” es el C. José Armando Larrazolo Porter, con domicilio en Avenida Paseo de la Reforma Número 403</p>
                <p>Despacho 601, Cuauhtémoc, Ciudad de México, C.P. 06500, horarios de atención de 9:00 a 18:00 horas de lunes a viernes, correo electrónico jalarrazolo@gapmet.com.mx teléfono 52022847.</p>

                <h3>VII. Medios para ejercer los derechos de acceso, rectificación, cancelación u oposición, de conformidad con lo dispuesto en esta Ley</h3>

                <p>Usted podrá ejercer sus derechos ARCO (acceso, rectificación, cancelación y/u oposición) contactando directamente al Encargado del Tratamiento de Datos Personales vía correo electrónico: jalarrazolo@gapmet.com.mx o agarcia@gapmet.com.mx</p>
                <p>La solicitud ARCO deberá contener y acompañar lo que señala la Ley en su artículo 29, tal como:</p>

                <ul>
                    <li>El nombre y domicilio del titular u otro medio para comunicarle la respuesta a su solicitud.</li>
                    <li>Los documentos que acrediten su identidad o, en su caso, la representación legal del titular.</li>
                    <li>La descripción clara y precisa de los datos personales respecto de los que se busca ejercer alguno de los derechos.</li>
                    <li>Cualquier otro elemento o documento que facilite la localización de los datos personales, así como cualquier otro documento que exija la legislación vigente al momento de la presentación de la solicitud.</li>
                </ul>

                <p>“La empresa” no estará obligada a cancelar los datos personales cuando se trate de alguno de los supuestos establecidos en la Ley, entre otros los siguientes: que se refiera a las partes de un contrato privado, social o administrativo y sean necesarios para su desarrollo y cumplimiento; deban ser tratados por disposición legal; se obstaculice actuaciones judiciales o administrativas vinculadas a obligaciones fiscales, la investigación y persecución de delitos o la actualización de sanciones administrativas; sean necesarios para proteger los intereses jurídicamente tutelados del titular; sean necesarios para realizar una acción en función del interés público; sean necesarios para cumplir una obligación legalmente adquirida por el titular y sean objeto de tratamiento para la prevención o para el diagnóstico médico o la gestión de servicios de salud, siempre que dicho tratamiento se realice por un profesional de la salud sujeto a un deber de secreto.</p>

                <p>Para tener mayor información respecto al ejercicio de sus derechos ARCO puede comunicarse directamente con el Encargado del Tratamiento de Datos Personales cuyos datos aparecen en el apartado VI del presente Aviso de Privacidad.</p>

                <h3>VIII. Transferencias de datos que se efectúen</h3>

                <p>Los datos personales jamás serán divulgados ni compartidos con terceros, en el entendido de que no se considera como tercero a las empresas que formen parte del mismo grupo corporativo de “La empresa” que requieran tener los datos personales para los fines antes mencionados. La única transferencia de datos a terceros será en todo caso para dar cumplimiento a obligaciones legales laborales, fiscales y seguridad social que deriven de la relación laboral o comercial con el titular de los datos personales.</p>

                <p>Por lo tanto, el titular al dar su consentimiento a este Aviso de Privacidad, o bien al no oponerse de forma manifiesta dentro del plazo de 15 (quince) días a partir de que tenga conocimiento del mismo acepta que sus datos personales sean tratados para las finalidades antes señaladas.</p>

                <h3>X. Revocación</h3>

                <p>El titular de los datos personales podrá revocar su consentimiento para el tratamiento de datos personales en cualquier momento, debiendo, únicamente para tales efectos, enviar una solicitud por escrito al Encargado de los Datos Personales, ya sea por mensajería al domicilio o al correo electrónico mencionados en el apartado VI del presente Aviso.</p>

                <p>Tendremos un plazo máximo de 20 días hábiles para informarle sobre la procedencia de la misma y si resulta procedente, se haga efectiva la misma dentro de los quince días hábiles siguientes a la fecha en que se comunica la respuesta al correo electrónico que nos proporcione para tales efectos.</p>

                <!-- <h3>X. Consentimiento del Titular</h3>
                Consiento que mis datos personales sensibles sean tratados conforme a los términos y condiciones del presente Aviso de Privacidad. Sí _______. No _______.
                    ________________________________
                    NOMBRE DEL EMPLEADO / CLIENTE FECHA DE INGRESO: 27/04/2017 -->

                <h3>XI. Cambios al Aviso de Privacidad</h3>

                <p>Nos reservamos el derecho de cambiar este Aviso de Privacidad en cualquier momento. Las modificaciones estarán disponibles en cualquiera de los siguientes medios:</p>

                <ul>
                    <li>Anuncios visibles en nuestros establecimientos (periódico mural) o módulos de Servicios al Personal.</li>
                    <li>En nuestra página de internet.</li>
                </ul>

                <p>No seremos responsables si usted no recibe la notificación de cambio en el Aviso de Privacidad
                si existiere algún problema con su transmisión de datos por internet.</p>

                <p>Fecha de la última actualización al presente aviso de privacidad: 27 de abril de 2017.</p>
            </div>
        </div>
    </div>
</section>

<?php include_once('footer.php'); ?>