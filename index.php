<?php 
$captcha = substr(md5(mt_rand()), 0, 4);
include_once("header.php"); ?>
	<div class="modal fade" id="modalAnuncio" tabindex="-1" role="dialog" aria-labelledby="modalAnuncioTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<img src="images/GAP-buen-fin.jpg" class="img-fluid desktop" alt="GAP Buen Fin">
					<img src="images/gap-buen-fin-vertical.jpg" class="img-fluid mobile" alt="GAP Buen Fin">
				</div>
			</div>
		</div>
	</div>
	
	<div id="main-carousel" class="main-carousel owl-carousel">
		<div class="item-carousel palacio">
			<!-- <img src="images/slider-1.jpg" alt="..."> -->
			<div class="carousel-caption">
				<h2>DEPARTAMENTOS <br>EN LA CDMX</h2>
				<div class="info">
					<div class="text">
						Aceptamos diferentes <br><strong>TIPOS DE CRÉDITO.</strong>
					</div>
					<div class="cta">
						<p>Precios desde <br><span>$720,000.00</span></p>
						<a href="<?php echo $path; ?>#developments" class="btn">VER DESARROLLOS</a>
					</div>
				</div>
			</div>
		</div>
		<div class="item-carousel angel">
			<!-- <img src="images/slider-2.jpg" alt="..."> -->
			<div class="carousel-caption">
				<h2>DEPARTAMENTOS <br>EN LA CDMX</h2>
				<div class="info">
					<div class="text">
						Aceptamos diferentes <br><strong>TIPOS DE CRÉDITO.</strong>
					</div>
					<div class="cta">
						<p>Precios desde <br><span>$720,000.00</span></p>
						<a href="#developments" class="btn">VER DESARROLLOS</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<img src="images/logo-bloque-home-gap-metropolitana.png" class="img-fluid mx-auto" alt="">
					<p>Es una <strong>empresa inmobiliaria</strong> que al día de hoy ha logrado desarrollar un nuevo producto dentro del sector de la vivienda económica.</p>
				</div>
			</div>
			<div class="row features">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
					<h3>30 <br>AÑOS</h3>
					<p>de experiencia</p>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
					<h3>30,000 <br>VIVIENDAS</h3>
					<p>construídas</p>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
					<h3>9 <br>DELEGACIONES</h3>
					<p>con desarrollos exitosos</p>
				</div>
			</div>
			<div class="row cta">
				<a href="nuestra-inmobiliaria" class="btn">VER MÁS</a>
			</div>
		</div>
	</section>

	<section class="on-sale">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-3 offset-xl-2 col-lg-3 offset-lg-1 col-md-4 offset-md-1 col-sm-12">
					<h2>DESARROLLOS <br>EN VENTA</h2>
					<p>Departamentos al mejor costo y con la mejor ubicación en el corazón de la Ciudad de México.</p>
				</div>
				<div class="col-xl-6 offset-xl-1 col-lg-6 offset-lg-2 col-md-6 offset-md-1 col-sm-12">
					<h3>¿POR QUÉ ELEGIRNOS?</h3>
					<ul>
						<li>
							<span class="icon-card"></span>
							<p>Aceptamos <strong>Infonavit & Fovissste</strong></p>
						</li>
						<li>
							<span class="icon-route"></span>
							<p>Ubicaciones</p>
						</li>
						<li>
							<span class="icon-store"></span>
							<p>Servicios a su alcance</p>
						</li>
						<li>
							<span class="icon-precio"></span>
							<p>Excelentes precios</p>
						</li>
					</ul>
				</div>
			</div>
		</div>		
	</section>

	<section class="developments" id="developments">
		<div class="container-fluid">
			<div class="row" id="desarrollos">
				<!-- <div class="col-xl-4 col-lg-6 col-md-6">

						<div class="info">
							<img src="images/edificio-el-mezquital.jpg" class="img-fluid" alt="MEZQUITAL">
							<a href="#">
								<span class="icon-link"></span>
								<h3>{protipo}</h3>
								<p>Mezquital #50, Col. Valle Gómez, <br>Del. Cuauhtémoc, CDMX.</p>
								<p><span class="icon-area"></span> 40 m<sup>2</sup> - 49 m<sup>2</sup></p>
								<span class="price">Desde $928,000.00 pesos</span>
							</a>
						</div>
				</div>-->
				<!-- <div class="col-xl-4 col-lg-6 col-md-6">
					<div class="info">
						<img src="images/edificio-el-mezquital.jpg" class="img-fluid" alt="MEZQUITAL">
						<a href="#">
							<span class="icon-link"></span>
							<h3>{protipo}</h3>
							<p>Mezquital #50, Col. Valle Gómez, <br>Del. Cuauhtémoc, CDMX.</p>
							<p><span class="icon-area"></span> 40 m<sup>2</sup> - 49 m<sup>2</sup></p>
							<span class="price">Desde $928,000.00 pesos</span>
						</a>
					</div>
				</div> -->
			</div>
		</div>
	</section>

	<!-- <section class="testimonials" id="testimoniales">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h2>TESTIMONIALES</h2>
					<p>Nuestros clientes avalan nuestros desarrollos.</p>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div id="testimonial-carousel" class="testimonial-carousel owl-carousel">
						<div class="item">
							<img src="images/testimonial-1.jpg" alt="Testimonial" class="photo img-responsive rounded-circle">
							<div class="text">
								<h6>HECTOR S. Y ANDREA H.</h6>
								<p>“Ampliamente recomendable los departamentos de Gap Metropolitana, empresa seria y comprometida”.</p>
							</div>
						</div>
						<div class="item">
							<img src="images/testimonial-2.jpg" alt="Testimonial" class="photo img-responsive rounded-circle ">
							<div class="text">
								<h6>DIANA MENDOZA</h6>
								<p>“Nunca pense que fuera tan fácil obtener mi propio departamento”.</p>
							</div>
						</div>
						<div class="item">
							<img src="images/testimonial-1.jpg" alt="Testimonial" class="photo img-responsive rounded-circle">
							<div class="text">
								<h6>HECTOR S. Y ANDREA H.</h6>
								<p>“Ampliamente recomendable los departamentos de Gap Metropolitana, empresa seria y comprometida”.</p>
							</div>
						</div>
						<div class="item">
							<img src="images/testimonial-2.jpg" alt="Testimonial" class="photo img-responsive rounded-circle">
							<div class="text">
								<h6>DIANA MENDOZA</h6>
								<p>“Nunca pense que fuera tan fácil obtener mi propio departamento”.</p>
							</div>
						</div>
						<div class="item">
							<img src="images/testimonial-1.jpg" alt="Testimonial" class="photo img-responsive rounded-circle">
							<div class="text">
								<h6>HECTOR S. Y ANDREA H.</h6>
								<p>“Ampliamente recomendable los departamentos de Gap Metropolitana, empresa seria y comprometida”.</p>
							</div>
						</div>
						<div class="item">
							<img src="images/testimonial-2.jpg" alt="Testimonial" class="photo img-responsive rounded-circle">
							<div class="text">
								<h6>DIANA MENDOZA</h6>
								<p>“Nunca pense que fuera tan fácil obtener mi propio departamento”.</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section> -->

	<section class="contact" id="contact">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-5 offset-xl-1 col-lg-12">
					<h2>CONTACTO</h2>
					<p>*Por favor llena los campos obligatorios.</p>
					<form id="form-contact" class="form-contact" action="<?php echo $path; ?>contacto" method="POST">
						<div class="form-group">
							<input type="text" class="form-control" id="nombre" name="nombre" placeholder="* Nombre Completo">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="telefono" name="telefono" placeholder="* Teléfono">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="email" name="email" placeholder="* Correo electrónico">
						</div>
						<div class="form-group">
							<select name="medio_contacto" id="medio_contacto" class="form-control">
								<option value="">*Medio de Contacto</option>
								<option value="Redes Sociales">Redes Sociales</option>
								<option value="Lona">Lona</option>
								<option value="Volante">Volante</option>
								<option value="Metro">Metro</option>
								<option value="Gallardete">Gallardete</option>
								<option value="Poster">Poster</option>
								<option value="Google">Google</option>
								<option value="Publicidad Digital">Publicidad Digital</option>
							</select>
						</div>
						<div class="form-group">
							<select name="desarrollo_interes" id="desarrollo_interes" class="form-control">
								<option value="">*Desarrollo de Interés</option>
								<option value="Mellado">Mellado</option>
								<option value="Mezquital">Mezquital</option>
								<option value="Arteaga">Arteaga</option>
								<option value="Clavijero">Clavijero</option>
								<option value="Sur 20 162">Sur 20 162</option>
								<option value="Sur 20 168">Sur 20 168</option>
								<option value="Sur 20 174">Sur 20 174</option>
							</select>
						</div>
						<div class="form-group">
							<select name="tipo_credito" id="tipo_credito" class="form-control">
								<option value="">* Tipo de crédito</option>
								<option value="INFONAVIT">INFONAVIT</option>
								<option value="FOVISSSTE">FOVISSSTE</option>
								<option value="Contado">Contado</option>
								<option value="Otro">Otro</option>
							</select>
						</div>
						<div class="form-group">
							<textarea class="form-control" id="message" name="message" rows="3" placeholder="* Mensaje"></textarea>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="captcha_value" name="captcha_value" placeholder="* Ingresa el código">
							<p class="completar"><?php echo $captcha; ?></p>
							<input type="hidden" id="captcha_code" name="captcha_code" value="<?php echo $captcha ?>">
						</div>
						<div class="form-group">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="accept_privacy_policies" name="accept_privacy_policies">
								<label class="form-check-label" for="gridCheck">
									He leído el <a href="<?php echo $path; ?>aviso-de-privacidad" target="_blank">Aviso de Privacidad.</a>
								</label>
							</div>
						</div>
						<button type="submit" class="btn btn-primary">ENVIAR</button>
					</form>
				</div>
				<div class="offices">
					<span class="icon-pin"></span>
					<h6>Oficinas de Ventas</h6>
					<ul>
					</ul>
				</div>
				<div class="col-xl-5 offset-xl-1 col-lg-6 col-md-6">
					<div id="map"></div>
				</div>
			</div>
		</div>
	</section>

<?php include_once("footer.php"); ?>

<script>
	jQuery(document).ready(function($){
		/*lectura de json */

		// var $template = "<div class='col-xl-4 col-lg-6 col-md-6'>"+
		// 				"<a href='{url}'>"+
		// 					"<img src='images{url_home}' class='img-fluid' alt='MEZQUITAL'>"+
		// 					"<div class='info'>"+
		// 						"<span class='icon-link'></span>"+
		// 						"<h3>{prototipo}</h3>"+
		// 						"<p>{direccion}</p>"+
		// 						"<p><span class='icon-area'></span> 40 m<sup>2</sup> - 49 m<sup>2</sup></p>"+
		// 						"<span class='price'>Desde ${precio} pesos</span>"+
		// 					"</div>"+
		// 				"</a>"+
		// 			"</div>";


		var $template = "<div class='col-xl-4 col-lg-6 col-md-6'>"+
							"<div class='info'>"+
								"<img src='images{url_home}' class='img-fluid' alt='MEZQUITAL'>"+
								"<a href='{url}'>"+
									"<span class='icon-link'></span>"+
									"<h3>{prototipo}</h3>"+
									"<p>{direccion}</p>"+
									"<p><span class='icon-area'>{rango_medidas}</p>"+
									"<span class='price'>{leyenda}</span>"+
								"</a>"+
							"</div>"+
						"</div>";

		var $template_oficinas = "<li>"+
									"<a href='#' data-lat='{lat}' data-long='{long}'>"+
										"<h5>{nombre_oficina}</h5>"+
										"<p>{direccion_oficina}</p>"+
									"</a>"+
								"</li>";


		$.each(protos, function(i, field){
			$("#desarrollos").append(
				$template.replace("{prototipo}",field.nombre)
				.replace("{direccion}",field.direccion)
				.replace("{url_home}",field.images.home)
				.replace("{url}","<?php echo $path; ?>"+field.url)
				.replace("{precio}",field.precio)
				.replace("{rango_medidas}",field.rango_medidas)
				.replace("{leyenda}",field.leyenda)
			);
		});

		$.each(oficinas,function(index,value){
			$(".offices ul").append(
				$template_oficinas.replace("{nombre_oficina}",value.nombre).replace("{direccion_oficina}",value.direccion).replace("{lat}",value.lat).replace("{long}",value.long)
			);
		});
		
	});
</script>