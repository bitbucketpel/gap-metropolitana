<?php 
$title = 'Nuestra Inmobiliaria | Gap Metropolitana Inmobiliaria';
$description = 'Somos una empresa desarrolladora de vivienda, con 30 años de experiencia en el sector inmobiliario, que ha participado exitosamente en la comercialización y construcción de viviendas en distintas delegaciones en la Ciudad de México';
$keywords = 'inmobiliaria, inmobiliarias df, empresas inmobiliarias, vivienda económica, desarrollos inmobiliarios, departamentos en venta, desarrollos inmobiliarios df, venta de departamentos, departamentos en venta df, proyectos inmobiliarios';
include_once('header.php'); ?>

<section class="header quienes-somos">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1>GAP METROPOLITANA</h1>
            </div>
        </div>
    </div>
</section>

<section class="quienes-somos-desc">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h2>QUIÉNES <span>SOMOS</span></h2>
                <p>Somos una <strong>empresa desarrolladora de vivienda, con 30 años de experiencia en el sector inmobiliario,</strong> que ha participado exitosamente en la comercialización y construcción de viviendas en distintas delegaciones en la Ciudad de México; y que al día de hoy ha logrado desarrollar un nuevo producto dentro del sector de la vivienda económica </p>
            </div>
        </div>
    </div>
</section>

<section class="presencia">
    <div class="container-fluid">
        <div class="row no-gutters align-items-center">
            <div class="col-xl-4 offset-xl-2 col-lg-5 offset-lg-1 col-md-6 col-sm-12">
                <h2>PRESENCIA</h2>
                <p>Actualmente tenemos desarrollos en las delegaciones:<strong> Miguel Hidalgo, Iztacalco, Venustiano Carranza, Azcapotzalco y Cuauhtémoc.</strong></p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <img src="images/gap-metropolitana-presencia.png" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>

<section class="section-title quienes-somos">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <h2>TRAYECTORIA</h2>
                <p>Primera Generación</p>
            </div>
        </div>
    </div>
</section>

<section class="list-trayectoria">
    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <img class="card-img-top img-fluid" src="images/trayectoria/lago-zirahuen.png" alt="Card image cap">
                    <div class="card-body">
                        <h4>Nov 2014</h4>
                        <p><span class="icon-building"></span><strong>ESPACIO LAGO ZIRAHUEN 180</strong></p>
                        <p><span class="icon-pin"></span>Lago Zirahuen #180, Col. Anáhuac I Sección, Del. Miguel Hidalgo, CDMX </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <img class="card-img-top img-fluid" src="images/trayectoria/angel-zimbron.png" alt="Card image cap">
                    <div class="card-body">
                        <h4>Abr 2015</h4>
                        <p><span class="icon-building"></span><strong>ESPACIO ANGEL ZIMBRÓN</strong></p>
                        <p><span class="icon-pin"></span>Ángel Zimbrón #15, Col. San Francisco, Del. Azcapotzalco, CDMX </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <img class="card-img-top img-fluid" src="images/trayectoria/cerrada-angeles.png" alt="Card image cap">
                    <div class="card-body">
                        <h4>May 2015</h4>
                        <p><span class="icon-building"></span><strong>ESPACIO CERRADA ANGELES 14</strong></p>
                        <p><span class="icon-pin"></span>Callejón de los Ángeles #14, Col. Santa Bárbara, Del. Azcapotzalco, CDMX</p>
                    </div>
                </div>
            </div>
            <div class="w-100"></div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <img class="card-img-top img-fluid" src="images/trayectoria/fresnos.png" alt="Card image cap">
                    <div class="card-body">
                        <h4>Jul 2015</h4>
                        <p><span class="icon-building"></span><strong>ESPACIO FRESNOS 139</strong></p>
                        <p><span class="icon-pin"></span>Cerrada de Fresnos #139, Col. San Martín Xochinahuac, Del. Azcapotzalco, CDMX</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <img class="card-img-top img-fluid" src="images/trayectoria/centlapatl.png" alt="Card image cap">
                    <div class="card-body">
                        <h4>Jul 2015</h4>
                        <p><span class="icon-building"></span><strong>ESPACIO CENTLAPATL 139</strong></p>
                        <p><span class="icon-pin"></span>Centlapatl #139, Col. San Martín Xochinahuac, Del. Azcapotzalco, CDMX</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <img class="card-img-top img-fluid" src="images/trayectoria/centlapatl-143.png" alt="Card image cap">
                    <div class="card-body">
                        <h4>Jul 2015</h4>
                        <p><span class="icon-building"></span><strong>ESPACIO CENTLAPATL 143</strong></p>
                        <p><span class="icon-pin"></span>Centlapatl #143, Col. San Martín Xochinahuac, Del. Azcapotzalco, CDMX</p>
                    </div>
                </div>
            </div>
            <div class="w-100"></div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <img class="card-img-top img-fluid" src="images/trayectoria/rinconada-zoquiapa.png" alt="Card image cap">
                    <div class="card-body">
                        <h4>Feb 2016</h4>
                        <p><span class="icon-building"></span><strong>ESPACIO RINCONADA ZOQUIPA 21</strong></p>
                        <p><span class="icon-pin"></span>Rincinada Zoquipa #21, Col. Merced Balbuena, Del. Venustiano Carranza, CDMX</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <img class="card-img-top img-fluid" src="images/trayectoria/guillermo-prieto.png" alt="Card image cap">
                    <div class="card-body">
                        <h4>May 2016</h4>
                        <p><span class="icon-building"></span><strong>ESPACIO GUILLERMO PRIETO 209</strong></p>
                        <p><span class="icon-pin"></span>Guillermo Prieto #209, Col. Magdalena Mixiuhca, Del. Venustiano Carranza, CDMX</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <img class="card-img-top img-fluid" src="images/trayectoria/manuel-m-flores.png" alt="Card image cap">
                    <div class="card-body">
                        <h4>Ene 2017</h4>
                        <p><span class="icon-building"></span><strong>ESPACIO MANUEL M. FLORES</strong></p>
                        <p><span class="icon-pin"></span>Manuel M. Flores #33-A, Col. Obrera, Del. Cuauhtémoc, CDMX</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-title">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <h2>NUEVA GENERACIÓN</h2>
            </div>
        </div>
    </div>
</section>

<section class="edificios-generacion">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div id="generation-carousel" class="new-generation-carousel owl-carousel">
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/lucas-alaman.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Oct 2016</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO LUCAS ALAMÁN 2200</strong></p>
                            <p><span class="icon-pin"></span>Lucas Alamán #2200, Col. Del Parque, Del. Venustiano Carranza, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/canito.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Ene 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO CAÑITO 39</strong></p>
                            <p><span class="icon-pin"></span>Cañito #39, Col. San Diego Ocoyoacac, Del. Miguel Hidalgo, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/moctezuma.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Nov 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO MOCTEZUMA 96</strong></p>
                            <p><span class="icon-pin"></span>Eje Central Lázaro Cárdenas #96, Col. Guerrero, Del. Cuauhtémoc, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/manzana.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Abr 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO MANZANA 9</strong></p>
                            <p><span class="icon-pin"></span>Manzana #9, Col. Santa Cruz Aviación, Del. Venustiano Carranza, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/lorenzo-buturini.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Jun 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO LORENZO BOTURINI 440</strong></p>
                            <p><span class="icon-pin"></span>Lorenzo Boturini #440, Col. Lorenzo Boturini, Del. Venustiano Carranza, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/ignacio-zaragoza.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Jul 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO IGNACIO ZARAGOZA 1025</strong></p>
                            <p><span class="icon-pin"></span>Ignacio Zaragoza #1025, Col. Agrícola Oriental, Del. Iztacalco, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/eje-central.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Jul 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO EJE CENTRAL 100</strong></p>
                            <p><span class="icon-pin"></span>Eje Central Lázaro Cárdenas #100, Col. Guerrero, Del. Cuauhtémoc, CDMX</p>
                        </div>
                    </div>
                    <!-- <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/calle-5.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Nov 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO CALLE 5 78</strong></p>
                            <p><span class="icon-pin"></span>Calle 5 #78, Col. Agrícola Pantitlán, Del. Iztacalco, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/calle-7.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Nov 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO CALLE 7 250</strong></p>
                            <p><span class="icon-pin"></span>Calle 7 #250, Col. Agrícola Pantitlán, Del. Iztacalco, CDMX</p>
                        </div>
                    </div> -->
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/jose-maria-estrada.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Nov 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO JOSÉ MARÍA ESTRADA 9</strong></p>
                            <p><span class="icon-pin"></span>Calle 53 #9, Col. Gral. Ignacio Zaragoza, Del. Venustiano Carranza, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/jesus-maria.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Dic 2017</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO JESÚS MARÍA 188</strong></p>
                            <p><span class="icon-pin"></span>Jesús María #188, Col. Centro, Del. Cuauhtémoc, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/zoltan.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4>Mar 2018</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO ZOLTAN KODALY 94</strong></p>
                            <p><span class="icon-pin"></span>Zoltan Kodaly #94, Col. San Simón Tolnáhuac, Del. Cuauhtémoc, CDMX</p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid" src="images/nueva-generacion/calle-5-nueva-generacion.png" alt="Card image cap">
                        <div class="card-body">
                            <h4>Mar 2018</h4>
                            <p><span class="icon-building"></span><strong>ESPACIO CALLE 5</strong></p>
                            <p><span class="icon-pin"></span>Calle 5 #78, Col. Agrícola Pantitlán, Del. Iztacalco, CDMX.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                <span class="icon-building"></span>
                <h2>DESARROLLOS EN VENTA</h2>
                <p>Conoce nuestros desarrollos</p>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                <a href="<?php echo $path; ?>#developments" class="btn desarrollos">VER DESARROLLOS</a>
            </div>
        </div>
    </div>
</section>

<?php include_once('footer.php'); ?>