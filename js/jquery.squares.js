(function($){
    "use strict";
    $.fn.squares = function(options){
        var self = $(this);
        var total = 0;
        var children;
        
        //default options
        var settings = $.extend({
            borderColor:"#a9a9a9",
            borderWidth:"1px",
            borderStyle:"solid",
            itemChildren:".items",
            columns:4,
            responsive:{}
        },options);

        return self.each(function() {
            // do something to each item matching the selector
            children = $(this).find(settings.itemChildren);
            total = children.length;

            children.css({
                "border-color":settings.borderColor,
                "border-width":settings.borderWidth
            })

            if(settings.responsive != null){
                responsive();
            }else{
                assignClass(children);
            }
       })

       function assignClass(element){
            element.each(function(index,value){
                var $module = (index + 1) % settings.columns;
                var $sobrantes = total % settings.columns;

                //cuando los elementos completan las columnas
                if($module != 0 & total >= settings.columns){
                    $(this).addClass("borderR");
                }

                //cuando la cuadricula no se completa
                if($sobrantes != 0){
                    if((index + 1) <= (total - $sobrantes)){
                        $(this).addClass("borderB");
                    }
                }if(total < settings.columns){
                    if((index + 1) < total){
                        $(this).addClass("borderR");
                    }
                }
                else{
                    //cuando queda exactamente por filas y columnas exactas
                    if((index + 1) <= (total - settings.columns)){
                        $(this).addClass("borderB");
                    }
                }
                
            })
        }

        function responsive(){
            $(window).on("resize load",function(){
                var viewport = $(window).width(),
                    match = -1;

                var overwrites = settings.responsive;

                $.each(overwrites, function(breakpoint) {
                    if (breakpoint <= viewport && breakpoint > match) {
                        match = Number(breakpoint);
                    }
                });

                $.each(overwrites[match],function(property,value){
                    eval("settings." + property+ "="+ value);
                });

                assignClass(children);
            });
        }
    };      
}(jQuery))