(function($) {
	"use strict";

	function handlePreloader() {
		if($('.preloader').length){
			$('.preloader').delay(500).fadeOut(500,goTo());
		}
	}

	function headerStyle() {
		if($('.main-header').length){
			var windowpos = $(window).scrollTop();
			if (windowpos >= 50) {
				$('.main-header').addClass('fixed-header');
				$('.scroll-to-top').fadeIn(300);
			} else {
				$('.main-header').removeClass('fixed-header');
				$('.scroll-to-top').fadeOut(300);
			}
		}
	}

	function goTo(){
		var link = window.location.href.split("#");
	
		if(link.length > 1){
			var ancla = "#"+link[1];
	
			jQuery('body,html').animate({				
				scrollTop: jQuery(ancla).offset().top - 120
			},1000);
	
			// setTimeout(function(){
			// 	jQuery('#menu-principal .expanded.active .sub-menu li').removeClass('active');
	
			// 	var links = jQuery( 'a[href*="#' + link[1]+ '"]' ).parent();
			// 	links.addClass('active');
			// },500)
		}
	}

	headerStyle();

	$(window).on('scroll', function() {
		headerStyle();
	});

	$(window).on('load', function() {
		handlePreloader();
	});

	if($('#main-carousel').length){
		$('#main-carousel').owlCarousel({
			items: 1,
			nav: true,
			navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
			autoplay:true,
			autoplayTimeout: 7000,
			freeDrag:true,
			loop:true
		});
	}

	if($('#testimonial-carousel').length){
		$('#testimonial-carousel').owlCarousel({
			items: 2,
			stagePadding:0,
			responsive:{
				0:{
					items:1
				},
				768:{
					items:2
				}
			}
		});
	}

	if($('#generation-carousel').length){
		$('#generation-carousel').owlCarousel({
			items: 3,
			nav:true,
			navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
			dots:true,
			margin:20,
			responsive:{
				0:{
					items:1
				},
				768:{
					items:2
				},
				1300:{
					items:3
				}
			}
		});
	}

	$(document).on("click",".offices a",(function(event){
		event.preventDefault();
		
		initMap($(this).data("lat"),$(this).data("long"));
	}));

	$(document).on("click",".oficinas a",(function(event){
		event.preventDefault();
		
		initMap($(this).data("lat"),$(this).data("long"));
	}));

	$('a').click(function (e) {
		var link = $(this).attr("href");
		var elementos = link.split("#");
        if(link == "#"){
			e.preventDefault();
		}else{
			if(elementos.length > 1){
				setTimeout(function() {
					goTo();	
				}, 40);
			}
		}
		
	});

	$("#form-contact").validate({
		rules:{
			nombre:"required",
			email:{
				required:true,
				email:true
			},
			medio_contacto:"required",
			telefono:{
				required:true
			},
			desarrollo_interes:"required",
			tipo_credito:"required",
			message:"required",
			captcha_value:"required",
			accept_privacy_policies:"required"
		},
		messages:{
			nombre:"Coloca tu nombre",
			telefono:{
				required:"Coloca tu número de teléfono",
				minlength:"El número debe de tener 10 dígitos"
			},
			email:{
				required:"Escribe tu correo electrónico",
				email:"Escribe un correo válido"
			},
			medio_contacto:"Selecciona una opción",
			desarrollo_interes:"Selecciona una opción",
			tipo_credito:"Selecciona una opción",
			message:"Escribe tu mensaje",
			captcha_value:"Escribe el código",
			accept_privacy_policies:"Acepta Póliticas de Privacidad"
		},
		errorPlacement: function(error, element) {
            if (element.attr("type") == "checkbox") {
                error.insertAfter($(element).closest(".form-check-input").closest('div'));
            }else if(element.attr("id") == "captcha_value"){
				error.insertAfter($(element).next(".completar"));
			} else {
                error.insertAfter(element);
            }
        },
		submitHandler: function(form,event) {
			//event.preventDefault();
			$(".btn-primary").attr('disabled','disabled');
			$(".btn-primary").html("<img src='../images/spin.gif' class='img-fluid' width='35px'>");
			form.submit();
		}
	});

	$('#modalAnuncio').modal();



})(window.jQuery);

function cuadricula(){
	var ventana_ancho = $(window).width();
	if(ventana_ancho < 992){
		var $col = 2;
	}else{
		var $col = 3;
	}
	//item
	var $item = $(".item-prototipo");

	//numero de elementos
	var $totalItems = $item.length;

	//elementos sobrantes del total
	var $sobrantes = $totalItems % $col;

	$item.each(function(index){
		$(this).addClass("borderB borderR");

		//saber si es divisible por columna
		var $module = (index + 1) % $col;
		
		if($module == 0){
			$(this).removeClass("borderR");
		}

		 //numero de elementos que sobran por fila 
		 if($sobrantes != 0){
			if((index + 1) > ($totalItems - $sobrantes)){
				$(this).removeClass("borderB");
			}
		}else{
			if((index + 1) > ($totalItems - $col)){
				$(this).removeClass("borderB");
			}
		}
	});

}

function initCorousel(){
	$("#desarrollo-carousel").owlCarousel({
		items:4,
		nav:true,
		navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
		margin:10,
		stagePadding:15,
		dots:false,
		responsive:{
			0:{
				items:2
			},
			480:{
				items:3
			},
			768:{
				items:4
			}
		}
	});

	//activación de primer elemento de carousel
	// $("#desarrollo-carousel .item-carousel").first().click();
	$(".list-items .item-prototipo").first().click();
	baguetteBox.run('.desarrollo-carousel');
}

function changeContent($element,$cadena,$valor){
	$($element).each(function() {
		var n = $(this).html().replace(
			$cadena,$valor)
		$(this).html(n);
	});
}

function assignValue($desarrollo,$ruta){
	   /*Informacion principal del desarrollo */
		changeContent(".espacios","{nombre_prototipo}",eval("protos."+$desarrollo+".nombre.toUpperCase()"));

		changeContent(".call-to-action","{nombre_prototipo}",eval("protos."+$desarrollo+".nombre.replace(/ /g,'-');"));

        changeContent(".espacios","{direccion}",eval("protos."+$desarrollo+".direccion"));
        changeContent(".espacios","{niveles}",eval("protos."+$desarrollo+".niveles"));
        changeContent(".prototipos","{total_deptos}",eval("protos."+$desarrollo+".departamentos"));
        changeContent(".espacios","{precio}",eval("protos."+$desarrollo+".precio"));
		changeContent(".espacios","{ubicacion_maps}",eval("protos."+$desarrollo+".ubicacion_maps"));
		
		/*Coloca las dos primeras amenidades en el detalle del desarrollo*/
        // var $cont = 0;
        // $.each(eval("protos."+$desarrollo+".amenidades"),function(i,value){
        //     $("#list-amenidades").append("<li><span class='"+value.icon+"'></span>"+i+"</li>");
        //     $cont++;
        //     if($cont == 2){
        //         return false;
        //     }
		// });

		$.each(eval("protos."+$desarrollo+".caracteristicas"),function(i,value){
			$("#list-amenidades").append("<li><span class='"+value.icon+"'></span>"+i+"</li>");
		});


		/*Galería de imagenes de amenidades de acuerdo a amenidades de desarrollos */
        
        $template_carousel = "<a href='{url_image_amenidades}' class='item-carousel' data-image='{url_image_amenidades}'>"+
                                "<img src='{url_image_amenidades}' class='img-fluid' alt=''>"+
                            "</a>";
        
        /*Esta es linea para agregar la fachada del desarrollo a la galeria */
        $("#desarrollo-carousel").append(
            $template_carousel.replace(/{url_image_amenidades}/g,$ruta+"/images/"+ eval("protos."+$desarrollo+".images.fachada"))
		);

		$(".espacios .image-view").css({
			"background-image":"url("+ $ruta+"/images/"+ eval("protos."+$desarrollo+".images.fachada")  +")"
		});


         $.each(eval("protos."+$desarrollo+".amenidades"),function(i,value){
            $.each(value.url_images,function(j,field){
                $("#desarrollo-carousel").append(
                    $template_carousel.replace(/{url_image_amenidades}/g,$ruta+"/images/"+ field)
                )
            })
         });

		 //función para visualizar las imagenes del carrusel en el visor
        //  $(".item-carousel").click(function(){
        //     var imagen = $(this).data("image");
        //     $(".image-view").css("background-image","url("+imagen+")");
		//  })
		 
		  /*coloca la lista de prototipos disponibles en los desarrollos */
		  var $template_protos = "<div class='item-prototipo' data-image='{image_proto}' data-meters='{medidas_proto}' data-name='{code_proto}' data-total='{num_deptos}' data-recamaras='{num_recamaras}'>"+
				"<h3>{code_proto}</h3>"+
				"<p><span class='icon-area'></span> {medidas_proto} m<sup>2</sup></p>"+
				"<p><strong>s/balcón({sin_balcon}) * c/balcón({con_balcon})</strong></p>"+
			"</div>";

		$.each(eval("protos."+$desarrollo+".prototipos"),function(i,value){
			$(".list-items").append(
				$template_protos.replace(/{code_proto}/g,i)
				.replace(/{medidas_proto}/g,value.metros)
				.replace("{con_balcon}",value.depto_con_balcon)
				.replace("{sin_balcon}",value.depto_sin_balcon)
				.replace("{image_proto}",value.image)
				.replace("{num_deptos}",(parseInt(value.depto_con_balcon) + parseInt(value.depto_sin_balcon)))
				.replace("{num_recamaras}",value.recamaras)
			);
		});
		
}