var protos =
    {
    "mezquital": {
        "nombre":"Mezquital",
        "direccion": "Mezquital #50, Col. Valle Gómez, Del. Cuauhtémoc, CDMX.",
        "ubicacion_maps":"https://www.google.com.mx/maps/place/Mezquital+50,+Valle+G%C3%B3mez,+06240+Ciudad+de+M%C3%A9xico,+CDMX/@19.4586067,-99.1259059,17z/data=!4m13!1m7!3m6!1s0x85d1f9154943bdd5:0x42b34b8138e3b88d!2sMezquital+50,+Valle+G%C3%B3mez,+06240+Ciudad+de+M%C3%A9xico,+CDMX!3b1!8m2!3d19.4586017!4d-99.1237172!3m4!1s0x85d1f9154943bdd5:0x42b34b8138e3b88d!8m2!3d19.4586017!4d-99.1237172",
        "niveles": "Planta baja y 5 niveles",
        "departamentos": "111",
        "precio":"Precios desde <br><span>$928,741.00</span>",
        "leyenda":"Precios desde $928,741.00 hasta $1,056,972.00",
        "rango_medidas":"36.064 m<sup>2</sup> - 47.492 m<sup>2</sup>",
        "caracteristicas":{
            "Planta Baja y 5 niveles":{
                "icon":"icon-building"
            },
            "Bodegas":{
                "icon":"icon-bodegas"
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage"
            },
            "Acceso controlado":{
                "icon":"icon-acceso-controlado"
            },
            "Elevador":{
                "icon":"icon-elevador"
            },  
            "Juegos Infantiles":{
                "icon":"icon-juegos"
            },
            "Local comercial":{
                "icon":"icon-store"
            }
        },
        "amenidades":{
            "Bodegas":{
                "icon":"icon-bodegas",
                "url_images":[
                    // "amenidades-gap-metro/01-bodega-gap-metropolitana-amenidades.jpg",
                    // "amenidades-gap-metro/02-bodega-gap-metropolitana-amenidades.jpg"
                    "mezquital/galeria/01-mezquital-comedor.jpg",
                    "mezquital/galeria/02-mezquital-recamara-a.jpg",
                    "mezquital/galeria/03-mezquital-recamara-b.jpg"
                ]
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage",
                "url_images":[
                    // "amenidades-gap-metro/03-estacionamiento-gap-metropolitana-amenidades.jpg",
                    // "amenidades-gap-metro/04-estacionamiento-gap-metropolitana-amenidades.jpg"
                    "mezquital/galeria/04-mezquital-sala-comedor.jpg",
                    "mezquital/galeria/05-mezquital-espacio.jpg"
                ]
            }
        },
        "prototipos":{
            "NM-01":{
                "metros":"47.492",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"mezquital/prototipos/NM-01.png"
            },
            "NM-02":{
                "metros":"45.542",
                "recamaras":"2",
                "depto_sin_balcon":"4",
                "depto_con_balcon":"1",
                "image":"mezquital/prototipos/NM-02.png"
            },
            "NM-03":{
                "metros":"43.591",
                "recamaras":"2",
                "depto_sin_balcon":"4",
                "depto_con_balcon":"1",
                "image":"mezquital/prototipos/NM-03.png"
            },
            "NM-04":{
                "metros":"41.64",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"mezquital/prototipos/NM-04.png"
            },
            "NM-05":{
                "metros":"42.375",
                "recamaras":"2",
                "depto_sin_balcon":"48",
                "depto_con_balcon":"0",
                "image":"mezquital/prototipos/NM-05.png"
            },
            "NM-06":{
                "metros":"42.299",
                "recamaras":"2",
                "depto_sin_balcon":"33",
                "depto_con_balcon":"0",
                "image":"mezquital/prototipos/NM-06.png"
            },
            "NM-07":{
                "metros":"45.566",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"mezquital/prototipos/NM-07.png"
            },
            "NM-08":{
                "metros":"47.143",
                "recamaras":"2",
                "depto_sin_balcon":"4",
                "depto_con_balcon":"1",
                "image":"mezquital/prototipos/NM-08.png"
            },
            "NM-09":{
                "metros":"36.064",
                "recamaras":"1",
                "depto_sin_balcon":"2",
                "depto_con_balcon":"0",
                "image":"mezquital/prototipos/NM-09.png"
            }
            
        },
        "images":{
            "home":"/edificio-el-mezquital.jpg",
            "fachada":"mezquital/fachada/mezquital.jpg"
        },
        "url":"departamentos-en-venta/mezquital"
      
    },
    "mellado":{
        "nombre":"Mellado",
        "direccion":"Mellado #51, Col. Valle Gómez, Del. Cuauhtémoc, CDMX.",
        "ubicacion_maps":"https://www.google.com.br/maps/place/Mellado+51,+Valle+G%C3%B3mez,+06240+Ciudad+de+M%C3%A9xico,+CDMX/@19.4587375,-99.1258794,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1f915379aed43:0xc8a307c26781b116!8m2!3d19.4587325!4d-99.1236907",
        "niveles":"Planta baja y 5 niveles",
        "departamentos":"195",
        "precio":"Precios desde <br><span>$957,524.00</span>",
        "leyenda":"Precios desde $957,524.00 hasta $1,135,924.00",
        "rango_medidas":"39.467 m<sup>2</sup> - 48.349 m<sup>2</sup>",
        "caracteristicas":{
            "Planta Baja y 5 niveles":{
                "icon":"icon-building"
            },
            "Bodegas":{
                "icon":"icon-bodegas"
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage"
            },
            "Acceso controlado":{
                "icon":"icon-acceso-controlado"
            },
            "Elevador":{
                "icon":"icon-elevador"
            },  
            "Juegos Infantiles":{
                "icon":"icon-juegos"
            },
            "Local comercial":{
                "icon":"icon-store"
            },
            "Gimnasio":{
                "icon":"icon-gimnasio"
            },
            "Salón de usos múltiples":{
                "icon":"icon-usos-multiples"
            },
            "Áreas comunes":{
                "icon":"icon-areas-comunes"
            }
        },
        "amenidades":{
            "Bodegas":{
                "icon":"icon-bodegas",
                "url_images":[
                    "amenidades-gap-metro/01-bodega-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/02-bodega-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/03-estacionamiento-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/04-estacionamiento-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Local comercial":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/07-local-comercial-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/08-local-comercial-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Gimnasio":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/12-gimnasio-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/13-gimnasio-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Salón de usos múltiples":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/16-usos-multiples-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/17-usos-multiples-gap-metropolitana-amenidades.jpg"
                ]
            }
        },
        "prototipos":{
            "NM-01":{
                "metros":"40.6",
                "recamaras":"1",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"mellado/prototipos/NM-01.png"
            },
            "NM-02":{
                "metros":"42.537",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"mellado/prototipos/NM-02.png"
            },
            "NM-03":{
                "metros":"44.474",
                "recamaras":"2",
                "depto_sin_balcon":"4",
                "depto_con_balcon":"1",
                "image":"mellado/prototipos/NM-03.png"
            },
            "NM-04":{
                "metros":"46.412",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"mellado/prototipos/NM-04.png"
            },
            "NM-05":{
                "metros":"48.349",
                "recamaras":"2",
                "depto_sin_balcon":"4",
                "depto_con_balcon":"1",
                "image":"mellado/prototipos/NM-05.png"
            },
            "NM-06":{
                "metros":"41.992",
                "recamaras":"2",
                "depto_sin_balcon":"55",
                "depto_con_balcon":"0",
                "image":"mellado/prototipos/NM-06.png"
            },
            "NM-07":{
                "metros":"42.144",
                "recamaras":"2",
                "depto_sin_balcon":"60",
                "depto_con_balcon":"0",
                "image":"mellado/prototipos/NM-07.png"
            },
            "NM-08":{
                "metros":"42.324",
                "recamaras":"2",
                "depto_sin_balcon":"30",
                "depto_con_balcon":"0",
                "image":"mellado/prototipos/NM-08.png"
            },
            "NM-09":{
                "metros":"47.845",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"mellado/prototipos/NM-09.png"
            },
            "NM-10":{
                "metros":"45.75",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"mellado/prototipos/NM-10.png"
            },
            "NM-11":{
                "metros":"43.656",
                "recamaras":"2",
                "depto_sin_balcon":"4",
                "depto_con_balcon":"1",
                "image":"mellado/prototipos/NM-11.png"
            },
            "NM-12":{
                "metros":"41.562",
                "recamaras":"2",
                "depto_sin_balcon":"4",
                "depto_con_balcon":"1",
                "image":"mellado/prototipos/NM-12.png"
            },
            "NM-13":{
                "metros":"39.467",
                "recamaras":"1",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"mellado/prototipos/NM-13.png"
            }
        },
        "images":{
            "home":"/edificio-mellado.jpg",
            "fachada":"mellado/fachada/mellado.jpg"
        },
        "url":"departamentos-en-venta/mellado"
    },
    "arteaga":{
        "nombre":"Arteaga",
        "direccion":"Arteaga #33, Col. Guerrero, Del. Cuauhtémoc, CDMX.",
        "ubicacion_maps":"https://www.google.com.br/maps/place/Arteaga+33,+Guerrero,+06300+Ciudad+de+M%C3%A9xico,+CDMX/@19.447903,-99.1447966,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1f926fee687b1:0xcb4c4059e482b104!8m2!3d19.447898!4d-99.1426079",
        "niveles":"Planta Baja y 2 Niveles",
        "departamentos":"50",
        "precio":"Precios desde <br><span>$976,600.00</span>",
        "leyenda":"Precios desde $976,600.00 hasta $988,600.00",
        "rango_medidas":"38.892 m<sup>2</sup> - 49.767 m<sup>2</sup>",
        "caracteristicas":{
            "Planta baja y 2 Niveles":{
                "icon":"icon-building"
            },
            "Bodegas":{
                "icon":"icon-bodegas"
            },
            "Cajones de Estacionamiento":{
                "icon":"icon-garage"
            }
        },  
        "amenidades":{
            "Bodegas":{
                "icon":"icon-bodegas",
                "url_images":[
                    "amenidades-gap-metro/01-bodega-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/02-bodega-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/03-estacionamiento-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/04-estacionamiento-gap-metropolitana-amenidades.jpg"
                ]
            }
        },
        "prototipos":{
            "NM-01":{
                "metros":"43.183",
                "recamaras":"2",
                "depto_sin_balcon":"29",
                "depto_con_balcon":"0",
                "image":"arteaga/prototipos/NM-01.png"
            },
            "NM-02":{
                "metros":"43.734",
                "recamaras":"2",
                "depto_sin_balcon":"3",
                "depto_con_balcon":"0",
                "image":"arteaga/prototipos/NM-02.png"
            },
            "NM-03":{
                "metros":"42.266",
                "recamaras":"2",
                "depto_sin_balcon":"3",
                "depto_con_balcon":"0",
                "image":"arteaga/prototipos/NM-03.png"
            },
            "NM-04":{
                "metros":"38.892",
                "recamaras":"1",
                "depto_sin_balcon":"3",
                "depto_con_balcon":"0",
                "image":"arteaga/prototipos/NM-04.png"
            },
            "NM-05":{
                "metros":"42.154",
                "recamaras":"1",
                "depto_sin_balcon":"3",
                "depto_con_balcon":"0",
                "image":"arteaga/prototipos/NM-05.png"
            },
            "NM-06":{
                "metros":"39.616",
                "recamaras":"1",
                "depto_sin_balcon":"3",
                "depto_con_balcon":"0",
                "image":"arteaga/prototipos/NM-06.png"
            },
            "NM-07":{
                "metros":"48.849",
                "recamaras":"2",
                "depto_sin_balcon":"3",
                "depto_con_balcon":"0",
                "image":"arteaga/prototipos/NM-07.png"
            },
            "NM-08":{
                "metros":"49.767",
                "recamaras":"2",
                "depto_sin_balcon":"3",
                "depto_con_balcon":"0",
                "image":"arteaga/prototipos/NM-08.png"
            },
        },
        "images":{
            "home":"/edificio-arteaga.jpg",
            "fachada":"arteaga/fachada/arteaga.jpg"
        },
        "url":"departamentos-en-venta/arteaga"
    },
    "clavijero":{
        "nombre":"Clavijero",
        "direccion":"Clavijero #40, Col. Esperanza, Del. Cuauhtémoc, CMDX.",
        "ubicacion_maps":"https://www.google.com.br/maps/place/Francisco+Javier+Clavijero+40,+Esperanza,+06840+Ciudad+de+M%C3%A9xico,+CDMX/@19.4205804,-99.1315585,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1fec5e6d255e9:0x8ae5b27adebb243c!8m2!3d19.4205754!4d-99.1293698",
        "niveles":"Planta Baja y 4 niveles",
        "departamentos":"109",
        "precio":"Precios desde<br><span> $1,036,000</span>",
        "leyenda":"Precios desde $1,036,000 hasta $1,126,000",
        "rango_medidas":"45.13 m<sup>2</sup> - 48.13 m<sup>2</sup>",
        "caracteristicas":{
            "Planta Baja y 4 niveles":{
                "icon":"icon-building"
            },
            "Planta Baja y 6 niveles":{
                "icon":"icon-building"
            },
            "Bodegas":{
                "icon":"icon-bodegas"
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage"
            },
            "Local Comercial":{
                "icon":"icon-store"
            },
            "Acceso Controlado":{
                "icon":"icon-acceso-controlado"
            },
            "Bici-estacionamiento":{
                "icon":"icon-biciestacionamiento"
            }
        },
        "amenidades":{
            "Bodegas":{
                "icon":"icon-bodegas",
                "url_images":[
                    "amenidades-gap-metro/01-bodega-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/02-bodega-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/03-estacionamiento-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/04-estacionamiento-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Local comercial":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/07-local-comercial-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/08-local-comercial-gap-metropolitana-amenidades.jpg"
                ]
            },
        },
        "prototipos":{
            "NM-01":{
                "metros":"47.49",
                "recamaras":"2",
                "depto_sin_balcon":"14",
                "depto_con_balcon":"0",
                "image":"clavijero/prototipos/NM-01.png"
            },
            "NM-02":{
                "metros":"45.79",
                "recamaras":"1",
                "depto_sin_balcon":"36",
                "depto_con_balcon":"0",
                "image":"clavijero/prototipos/NM-02.png"
            },
            "NM-03":{
                "metros":"45.13",
                "recamaras":"2",
                "depto_sin_balcon":"36",
                "depto_con_balcon":"0",
                "image":"clavijero/prototipos/NM-03.png"
            },
            "NM-04":{
                "metros":"46.00",
                "recamaras":"2",
                "depto_sin_balcon":"20",
                "depto_con_balcon":"0",
                "image":"clavijero/prototipos/NM-04.png"
            },
            "NM-05":{
                "metros":"48.13",
                "recamaras":"2",
                "depto_sin_balcon":"3",
                "depto_con_balcon":"0",
                "image":"clavijero/prototipos/NM-05.png"
            },
        },
        "images":{
            "home":"/clavijero/fachada/edificio-clavijero.jpg",
            "fachada":"clavijero/fachada/clavijero.jpg"
        },
        "url":"departamentos-en-venta/clavijero"
    },
    // "calle5":{
    //     "nombre":"Calle 5",
    //     "direccion":"Calle 5 #78, Col. Agrícola Pantitlán, Del. Iztacalco, CDMX. ",
    //     "ubicacion_maps":"https://www.google.com.br/maps/place/Calle+5+78,+Agr%C3%ADcola+Pantitl%C3%A1n,+08100+Ciudad+de+M%C3%A9xico,+CDMX/@19.4030309,-99.0629321,16.71z/data=!4m5!3m4!1s0x85d1fc5993be78cf:0x63f2b0830f0fafb9!8m2!3d19.4039747!4d-99.0617014",
    //     "niveles":"Planta Baja y 4 niveles",
    //     "departamentos":"149",
    //     "precio":"Precios desde <br><span>$666,703.00</span>",
    //     "leyenda":"Precios desde $666,703.00",
    //     "rango_medidas":"39.008 m<sup>2</sup> - 47.196 m<sup>2</sup>",
    //     "caracteristicas":{
    //         "Planta baja y 4 niveles":{
    //             "icon":"icon-building"
    //         },
    //         "Áreas comunes":{
    //             "icon":"icon-areas-comunes"
    //         },
    //         "Cajones de estacionamiento":{
    //             "icon":"icon-garage"
    //         }
    //     },
    //     "amenidades":{
    //         "Áreas comunes":{
    //             "icon":"icon-bodegas",
    //             "url_images":[
    //                 "amenidades-gap-metro/05-areas-comunes-gap-metropolitana-amenidades.jpg",
    //                 "amenidades-gap-metro/06-areas-comunes-gap-metropolitana-amenidades.jpg"
    //             ]
    //         },
    //         "Cajones de estacionamiento":{
    //             "icon":"icon-garage",
    //             "url_images":[
    //                 "amenidades-gap-metro/03-estacionamiento-gap-metropolitana-amenidades.jpg",
    //                 "amenidades-gap-metro/04-estacionamiento-gap-metropolitana-amenidades.jpg"
    //             ]
    //         }
    //     },
    //     "prototipos":{
    //         "NM-01":{
    //             "metros":"45.016",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"62",
    //             "depto_con_balcon":"2",
    //             "image":"calle5/prototipos/NM-01.png"
    //         },
    //         "NM-02":{
    //             "metros":"42.022",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"70",
    //             "depto_con_balcon":"0",
    //             "image":"calle5/prototipos/NM-02.png"
    //         },
    //         "NM-03":{
    //             "metros":"47.196",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"5",
    //             "depto_con_balcon":"0",
    //             "image":"calle5/prototipos/NM-03.png"
    //         },
    //         "NM-04":{
    //             "metros":"39.008",
    //             "recamaras":"1",
    //             "depto_sin_balcon":"10",
    //             "depto_con_balcon":"0",
    //             "image":"calle5/prototipos/NM-04.png"
    //         },
    //     },
    //     "images":{
    //         "home":"/edificio-calle5.jpg",
    //         "fachada":"calle5/fachada/calle-5.jpg"
    //     },
    //     "url":"departamentos-en-venta/calle-5"
    // },
    // "calle7":{
    //     "nombre":"Calle 7",
    //     "direccion":"Calle 7 #250, Col. Agrícola Pantitlán, Del. Iztacalco, CDMX.",
    //     "ubicacion_maps":"https://www.google.com.br/maps/place/Calle+Siete+250,+Agr%C3%ADcola+Pantitl%C3%A1n,+08100+Ciudad+de+M%C3%A9xico,+CDMX/@19.4099507,-99.0595197,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1fc5f6331945f:0xcc011f8ca5b8298!8m2!3d19.4099457!4d-99.057331",
    //     "niveles":"Planta Baja y 4 niveles",
    //     "departamentos":"122",
    //     "precio":"Precios desde <br><span>$720,000.00</span>",
    //     "leyenda":"Precios desde $720,000.00",
    //     "rango_medidas":"40.656 m<sup>2</sup> - 46.728 m<sup>2</sup>",
    //     "caracteristicas":{
    //         "Planta baja y 4 niveles":{
    //             "icon":"icon-building"
    //         },
    //         "Bodegas":{
    //             "icon":"icon-bodegas"
    //         },
    //         "Cajones de estacionamiento":{
    //             "icon":"icon-garage"
    //         },
    //         "Ecotecnologías":{
    //             "icon":"icon-ecotecnologia"
    //         }
    //     },
    //     "amenidades":{
    //         "Bodegas":{
    //             "icon":"icon-bodegas",
    //             "url_images":[
    //                 "amenidades-gap-metro/01-bodega-gap-metropolitana-amenidades.jpg",
    //                 "amenidades-gap-metro/02-bodega-gap-metropolitana-amenidades.jpg"
    //             ]
    //         },
    //         "Cajones de estacionamiento":{
    //             "icon":"icon-garage",
    //             "url_images":[
    //                 "amenidades-gap-metro/03-estacionamiento-gap-metropolitana-amenidades.jpg",
    //                 "amenidades-gap-metro/04-estacionamiento-gap-metropolitana-amenidades.jpg"
    //             ]
    //         }
    //     },
    //     "prototipos":{
    //         "NM-01":{
    //             "metros":"46.728",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"3",
    //             "depto_con_balcon":"1",
    //             "image":"calle7/prototipos/NM-01.jpg"
    //         },
    //         "NM-02":{
    //             "metros":"45.169",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"5",
    //             "depto_con_balcon":"0",
    //             "image":"calle7/prototipos/NM-02.png"
    //         },
    //         "NM-03":{
    //             "metros":"45.360",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"5",
    //             "depto_con_balcon":"0",
    //             "image":"calle7/prototipos/NM-03.png"
    //         },
    //         "NM-04":{
    //             "metros":"40.656",
    //             "recamaras":"1",
    //             "depto_sin_balcon":"8",
    //             "depto_con_balcon":"0",
    //             "image":"calle7/prototipos/NM-04.png"
    //         },
    //         "NM-05":{
    //             "metros":"45.941",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"10",
    //             "depto_con_balcon":"0",
    //             "image":"calle7/prototipos/NM-05.png"
    //         },
    //         "NM-06":{
    //             "metros":"45.165",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"10",
    //             "depto_con_balcon":"0",
    //             "image":"calle7/prototipos/NM-06.png"
    //         },
    //         "NM-07":{
    //             "metros":"45.028",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"7",
    //             "depto_con_balcon":"1",
    //             "image":"calle7/prototipos/NM-07.png"
    //         },
    //         "NM-08":{
    //             "metros":"45.474",
    //             "recamaras":"2",
    //             "depto_sin_balcon":"68",
    //             "depto_con_balcon":"4",
    //             "image":"calle7/prototipos/NM-08.png"
    //         }
    //     },
    //     "images":{
    //         "home":"/edificio-calle7.jpg",
    //         "fachada":"calle7/fachada/calle-7.jpg"
    //     },
    //     "url":"departamentos-en-venta/calle-7"
    // },
    "sur162":{
        "nombre":"Sur 20 162",
        "direccion":"Sur 20 #162, Col. Agrícola Oriental, Del. Iztacalco, CDMX.",
        "ubicacion_maps":"https://www.google.com.mx/maps/place/Sur+20+162,+Agr%C3%ADcola+Oriental,+08500+Ciudad+de+M%C3%A9xico,+CDMX/@19.3930529,-99.0807885,17z/data=!4m13!1m7!3m6!1s0x85d1fc4a71f344fb:0x956d9246745e1192!2sSur+20+162,+Agr%C3%ADcola+Oriental,+08500+Ciudad+de+M%C3%A9xico,+CDMX!3b1!8m2!3d19.3930479!4d-99.0785998!3m4!1s0x85d1fc4a71f344fb:0x956d9246745e1192!8m2!3d19.3930479!4d-99.0785998",
        "niveles":"Planta Baja y 4 niveles",
        "departamentos":"118",
        "precio":"Precios desde <br><span>$975,000.00</span>",
        "leyenda":"Precios desde $975,000.00",
        "rango_medidas":"39 m<sup>2</sup> - 46 m<sup>2</sup>",
        "caracteristicas":{
            "Planta baja y 5 niveles":{
                "icon":"icon-building"
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage"
            },
            "Elevador":{
                "icon":"icon-elevador"
            },
            "Acceso controlado":{
                "icon":"icon-acceso-controlado"
            },
            "Bici-estacionamiento":{
                "icon":"icon-biciestacionamiento"
            },
            "Áreas comunes":{
                "icon":"icon-areas-comunes"
            }
        },
        "amenidades":{
            "Bodegas":{
                "icon":"icon-bodegas",
                "url_images":[
                    "amenidades-gap-metro/01-bodega-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/02-bodega-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/03-estacionamiento-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/04-estacionamiento-gap-metropolitana-amenidades.jpg"
                ]
            }
        },
        "prototipos":{
            "NM-01":{
                "metros":"46.612",
                "recamaras":"2",
                "depto_sin_balcon":"6",
                "depto_con_balcon":"0",
                "image":"sur162/prototipos/NM-01.png"
            },
            "NM-02":{
                "metros":"46.147",
                "recamaras":"2",
                "depto_sin_balcon":"6",
                "depto_con_balcon":"0",
                "image":"sur162/prototipos/NM-02.png"
            },
            "NM-03":{
                "metros":"44.406",
                "recamaras":"2",
                "depto_sin_balcon":"3",
                "depto_con_balcon":"2",
                "image":"sur162/prototipos/NM-03.png"
            },
            "NM-04":{
                "metros":"42.665",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"sur162/prototipos/NM-04.png"
            },
            "NM-05":{
                "metros":"43.949",
                "recamaras":"2",
                "depto_sin_balcon":"90",
                "depto_con_balcon":"0",
                "image":"sur162/prototipos/NM-05.png"
            },
            "NM-06":{
                "metros":"49.935",
                "recamaras":"2",
                "depto_sin_balcon":"6",
                "depto_con_balcon":"0",
                "image":"sur162/prototipos/NM-06.png"
            }
        },
        "images":{
            "home":"/edificio-sur-162.jpg",
            "fachada":"sur162/fachada/sur-162.jpg"
        },
        "url":"departamentos-en-venta/sur-162"
    },
    "sur168":{
        "nombre":"Sur 20 168",
        "direccion":"Sur 20 #168, Col. Agrícola Oriental, Del. Iztacalco, CDMX.",
        "ubicacion_maps":"https://www.google.com.mx/maps/place/Sur+20+162,+Agr%C3%ADcola+Oriental,+08500+Ciudad+de+M%C3%A9xico,+CDMX/@19.3930529,-99.0807885,17z/data=!4m13!1m7!3m6!1s0x85d1fc4a71f344fb:0x956d9246745e1192!2sSur+20+162,+Agr%C3%ADcola+Oriental,+08500+Ciudad+de+M%C3%A9xico,+CDMX!3b1!8m2!3d19.3930479!4d-99.0785998!3m4!1s0x85d1fc4a71f344fb:0x956d9246745e1192!8m2!3d19.3930479!4d-99.0785998",
        "niveles":"Planta Baja y 5 niveles",
        "departamentos":"118",
        "precio":"Precios desde <br><span>$971,000.00</span>",
        "leyenda":"Precios desde $971,000.00",
        "rango_medidas":"39 m<sup>2</sup> - 46 m<sup>2</sup>",
        "caracteristicas":{
            "Planta baja y 5 niveles":{
                "icon":"icon-building"
            },
            "Bodegas":{
                "icon":"icon-ecotecnologia"
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage"
            },
            "Elevador":{
                "icon":"icon-elevador"
            },
            "Acceso controlado":{
                "icon":"icon-acceso-controlado"
            },
            "Bici-estacionamiento":{
                "icon":"icon-biciestacionamiento"
            },
            "Áreas comunes":{
                "icon":"icon-areas-comunes"
            }
        },
        "amenidades":{
            "Bodegas":{
                "icon":"icon-bodegas",
                "url_images":[
                    "amenidades-gap-metro/01-bodega-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/02-bodega-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/03-estacionamiento-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/04-estacionamiento-gap-metropolitana-amenidades.jpg"
                ]
            }
        },
        "prototipos":{
            "NM-01":{
                "metros":"48.355",
                "recamaras":"2",
                "depto_sin_balcon":"6",
                "depto_con_balcon":"0",
                "image":"sur168/prototipos/NM-01.png"
            },
            "NM-02":{
                "metros":"45.424",
                "recamaras":"2",
                "depto_sin_balcon":"6",
                "depto_con_balcon":"0",
                "image":"sur168/prototipos/NM-02.png"
            },
            "NM-03":{
                "metros":"43.764",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"sur168/prototipos/NM-03.png"
            },
            "NM-04":{
                "metros":"42.542",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"sur168/prototipos/NM-04.png"
            },
            "NM-05":{
                "metros":"44.014",
                "recamaras":"2",
                "depto_sin_balcon":"6",
                "depto_con_balcon":"0",
                "image":"sur168/prototipos/NM-05.png"
            },
            "NM-06":{
                "metros":"43.554",
                "recamaras":"2",
                "depto_sin_balcon":"78",
                "depto_con_balcon":"0",
                "image":"sur168/prototipos/NM-06.png"
            },
            "NM-07":{
                "metros":"43.722",
                "recamaras":"2",
                "depto_sin_balcon":"12",
                "depto_con_balcon":"0",
                "image":"sur168/prototipos/NM-07.png"
            }
        },
        "images":{
            "home":"/edificio-sur-168.jpg",
            "fachada":"sur168/fachada/sur-168.jpg"
        },
        "url":"departamentos-en-venta/sur-168"
    },
    "sur174":{
        "nombre":"Sur 20 174",
        "direccion":"Sur 20 #174, Col. Agrícola Oriental, Del. Iztacalco, CDMX.",
        "ubicacion_maps":"https://www.google.com.mx/maps/place/Sur+20+162,+Agr%C3%ADcola+Oriental,+08500+Ciudad+de+M%C3%A9xico,+CDMX/@19.3930529,-99.0807885,17z/data=!4m13!1m7!3m6!1s0x85d1fc4a71f344fb:0x956d9246745e1192!2sSur+20+162,+Agr%C3%ADcola+Oriental,+08500+Ciudad+de+M%C3%A9xico,+CDMX!3b1!8m2!3d19.3930479!4d-99.0785998!3m4!1s0x85d1fc4a71f344fb:0x956d9246745e1192!8m2!3d19.3930479!4d-99.0785998",
        "niveles":"Planta Baja y 5 niveles",
        "departamentos":"118",
        "precio":"Precios desde <br><span>$976,000.00</span>",
        "leyenda":"Precios desde $976,000.00",
        "rango_medidas":"39 m<sup>2</sup> - 46 m<sup>2</sup>",
        "caracteristicas":{
            "Planta baja y 5 niveles":{
                "icon":"icon-building"
            },
            "Bodegas":{
                "icon":"icon-bodegas"
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage"
            },
            "Elevador":{
                "icon":"icon-elevador"
            },
            "Acceso controlado":{
                "icon":"icon-acceso-controlado"
            },
            "Bici-estacionamiento":{
                "icon":"icon-biciestacionamiento"
            },
            "Áreas Comunes":{
                "icon":"icon-areas-comunes"
            }
        },
        "amenidades":{
            "Bodegas":{
                "icon":"icon-bodegas",
                "url_images":[
                    "amenidades-gap-metro/01-bodega-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/02-bodega-gap-metropolitana-amenidades.jpg"
                ]
            },
            "Cajones de estacionamiento":{
                "icon":"icon-garage",
                "url_images":[
                    "amenidades-gap-metro/03-estacionamiento-gap-metropolitana-amenidades.jpg",
                    "amenidades-gap-metro/04-estacionamiento-gap-metropolitana-amenidades.jpg"
                ]
            }
        },
        "prototipos":{
            "NM-01":{
                "metros":"48.355",
                "recamaras":"2",
                "depto_sin_balcon":"6",
                "depto_con_balcon":"0",
                "image":"sur174/prototipos/NM-01.png"
            },
            "NM-02":{
                "metros":"45.424",
                "recamaras":"2",
                "depto_sin_balcon":"4",
                "depto_con_balcon":"2",
                "image":"sur174/prototipos/NM-02.png"
            },
            "NM-03":{
                "metros":"43.764",
                "recamaras":"2",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"sur174/prototipos/NM-03.png"
            },
            "NM-04":{
                "metros":"42.542",
                "recamaras":"1",
                "depto_sin_balcon":"5",
                "depto_con_balcon":"0",
                "image":"sur174/prototipos/NM-04.png"
            },
            "NM-05":{
                "metros":"44.014",
                "recamaras":"2",
                "depto_sin_balcon":"6",
                "depto_con_balcon":"0",
                "image":"sur174/prototipos/NM-05.png"
            },
            "NM-06":{
                "metros":"43.554",
                "recamaras":"2",
                "depto_sin_balcon":"78",
                "depto_con_balcon":"0",
                "image":"sur174/prototipos/NM-06.png"
            },
            "NM-07":{
                "metros":"43.722",
                "recamaras":"2",
                "depto_sin_balcon":"12",
                "depto_con_balcon":"0",
                "image":"sur174/prototipos/NM-07.png"
            }
        },
        "images":{
            "home":"/edificio-sur-174.jpg",
            "fachada":"sur174/fachada/sur-174.jpg"
        },
        "url":"departamentos-en-venta/sur-174"
    }        
  }
;

/*Oficinas en venta */

var oficinas = {
    "boleo":{
        "nombre":"BOLEO",
        "direccion":"Calle Boleo #151, Esq. Plomo, Col. Valle<br> Gómez, Del. Cuauhtémoc, CDMX, C.P. 15210.<br> Tel: 55-78-07-79",
        "lat":"19.4570442",
        "long":"-99.1235537"
    },
    "arteaga":{
        "nombre":"ARTEAGA",
        "direccion":"Calle Arteaga #147, Local A, Esq. Lerdo, <br>Col. Guerrero, Del. Cuauhtémoc, CDMX, C.P. 06300. <br>Tel: 59-25-69-85",
        "lat":"19.448145",
        "long":"-99.143999"
    },
    // "calle5":{
    //     "nombre":"CALLE 5",
    //     "direccion":"Calle 5 #78, Col. Agrícola Pantitlán, <br>Del. Iztacalco, C.P. 08100, <br>Tel: 91- 54-61-08",
    //     "lat":"19.4039797",
    //     "long":"-99.0638901"
    // },
    // "calle7":{
    //     "nombre":"CALLE 7",
    //     "direccion":"Calle 7 #250, Col. Agrícola Pantitlán, <br>Del. Iztacalco, C.P. 08100, <br>Tel: 55-11-26-39",
    //     "lat":"19.4098381",
    //     "long":"-99.0577367"
    // },
    "fray-servando":{
        "nombre":"FRAY SERVANDO",
        "direccion":" Fray Servando #322-B, Col. Centro, <br>Del. Cuauhtémoc, CDMX.<br>Tel: 21-21-31-08",
        "lat":"19.423396",
        "long":"-99.128567"
    },
    "sur-174":{
        "nombre":"Sur 20 #191",
        "direccion":"Sur 20 #191, Col. Agrícola Oriental, <br>Del. Iztacalco, CDMX. <br>Tel: 15-45-53-85",
        "lat":"19.3931869",
        "long": "-99.0797918"
    }
};