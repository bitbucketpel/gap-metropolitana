<?php 
$title = 'Departamentos en Venta | Gap Metropolitana Inmobiliaria';
$description = 'Empresa inmobiliaria con 30 años de experiencia y 30,000 viviendas contruídas dentro del sector de vivienda económica.';
$keywords = 'inmobiliaria, inmobiliarias df, empresas inmobiliarias, vivienda económica, desarrollos inmobiliarios, departamentos en venta';
include_once '../header.php'; ?>

<section class="header">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1>DESARROLLOS EN VENTA</h1>
            </div>
        </div>
    </div>
</section>

<section class="espacios">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-sm-12 col-12">
                <h2>ESPACIO <span>{nombre_prototipo}</span></h2>
                <div class="direccion">
                    <span class="icon-pin"></span>{direccion}
                    <a href="{ubicacion_maps}" target="_blank">
                        Ver ubicación
                    </a>
                </div>
                <ul id="list-amenidades">
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="image-view">
                    <div class="info">
                        <div class="text">
                            Créditos <strong>INFONAVIT, FOVISSSTE & BANCARIO</strong>
                        </div>
                        <div class="cta">
                            <p>{precio}</p>
                        </div>
                    </div>
                    <!-- <img src="<?php echo $path; ?>images/preview-imagen-desarrollo.png" class="img-fluid preview" alt=""> -->
                </div>
                <div id="desarrollo-carousel" class="desarrollo-carousel owl-carousel">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-title">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <h2>PROTOTIPOS</h2>
            </div>
        </div>
    </div>
</section>

<section class="prototipos">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-5 order-xl-1 col-lg-5 order-lg-1 col-md-5 order-md-1 col-sm-12 order-sm-2 col-12 order-2">
                <div class="list-items">
                    
                </div>
                <div class="total-deptos">
                    <h3>Total de departamentos: {total_deptos}</h3>
                </div>
                <div class="distribucion">
                    <h3>Distribución</h3>
                    <ul>
                        <li class="num_recamaras"></li>
                        <li><span class="icon-kitchen"></span>Cocina</li>
                        <li><span class="icon-toilet"></span>Baño</li>
                        <li class="num_deptos"></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-7 order-xl-2 col-lg-7 order-lg-2 col-md-7 order-md-2 col-sm-12 order-sm-1 col-12 order-1">
                <div class="info-prototipo">
                    <h3 class="nombre">NM-01</h3>
                    <p class="medidas"><span class="icon-area"></span> 40.600 m<sup>2</sup></p>
                    <img src="<?php echo $path; ?>images/prototipos-1.png" class="img-fluid image-prototipo" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-7 col-sm-12 col-12">
                <span class="icon-pin"></span>
                <h2>CONTACTO</h2>
                <p>Visita Nuestras oficinas de ventas</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-5 col-sm-12 col-12">
                <a href="<?php echo $path;?>contacto?desarrollo={nombre_prototipo}" class="btn">CONTACTAR</a>
            </div>
        </div>
    </div>
</section>

<?php include_once '../footer.php'; ?>

<script type="text/javascript">
    jQuery(document).ready(function($){
        assignValue("sur174","<?php echo $path;?>");

        $(".list-items .item-prototipo").click(function(){
            var $spin = "<div class='overlay'><img src='<?php echo $path; ?>images/preloader.svg' class='img-responsive'></div>";
            $(".info-prototipo").prepend($spin);

            $(".list-items .item-prototipo").removeClass("active");
            $(this).addClass("active");
            
            $(".info-prototipo .nombre").html($(this).data("name"));
            $(".info-prototipo .medidas").html("<span class='icon-area'></span> "+$(this).data("meters")+" m<sup>2</sup>");
            $(".info-prototipo .image-prototipo").attr("src","<?php echo $path;?>/images/"+ $(this).data("image"));
            $(".num_deptos").empty().append('<span class="icon-building"></span>Deptos ('+ $(this).data("total")+')');
            $(".num_recamaras").empty().append('<span class="icon-room"></span>Recámaras (' + $(this).data("recamaras") + ')'); 

            var tmpImg = new Image() ;
            tmpImg.src = $('.image-prototipo').attr('src') ;
            tmpImg.onload = function() {
                setTimeout(function(){
                    $(".overlay").remove();
                }, 400);
               
            } ; 

        });

       /*Función cuadricula */
       setTimeout(function(){
            cuadricula();
            initCorousel();
       }, 500);

        $("#list-amenidades").squares({
            itemChildren:"li",
            responsive:{
                0:{
                    columns:1
                },
                480:{
                    columns:2
                },
                768:{
                    columns:3
                },
                992:{
                    columns:4
                }
            }
        });

    });

    
</script>