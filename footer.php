	<footer>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-5 offset-xl-0 col-lg-5 offset-lg-0 col-md-6 offset-md-1 col-sm-8 col-8">
					<img src="<?php echo $path; ?>images/gap-metropolitana-logo-alt.png" class="img-responsive" alt="GAP Metropolitana">
					<div class="division"></div>
					<img src="<?php echo $path; ?>images/tu-espacio-en-la-ciudad.png" class="img-responsive" alt="Tu espacio, tu ciudad">
				</div>
				<div class="col-xl-2 col-lg-2 col-md-5 offset-md-1 col-sm-5 offset-sm-0 col-12">
					<h4>DESARROLLOS</h4>
					<ul class="desarrollos">
						<li><a href="<?php echo $path; ?>departamentos-en-venta/mezquital">Mezquital</a></li>
						<li><a href="<?php echo $path; ?>departamentos-en-venta/mellado">Mellado</a></li>
						<li><a href="<?php echo $path; ?>departamentos-en-venta/arteaga">Arteaga</a></li>
						<li><a href="<?php echo $path; ?>departamentos-en-venta/clavijero">Clavijero</a></li>
						<!-- <li><a href="<?php echo $path; ?>departamentos-en-venta/calle-5">Calle 5</a></li> -->
						<!-- <li><a href="<?php echo $path; ?>departamentos-en-venta/calle-7">Calle 7</a></li> -->
						<li><a href="<?php echo $path; ?>departamentos-en-venta/sur-162">Sur 162</a></li>
						<li><a href="<?php echo $path; ?>departamentos-en-venta/sur-168">Sur 168</a></li>
						<li><a href="<?php echo $path; ?>departamentos-en-venta/sur-174">Sur 174</a></li>
					</ul>
				</div>
				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
					<div class="division"></div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
					<h4><a href="<?php echo $path; ?>nuestra-inmobiliaria">GAP METROPOLITANA</a></h4>
					<h4><a href="<?php echo $path; ?>contacto">CONTACTO</a></h4>
					<!-- <h4>SÍGUENOS</h4>
					<ul class="social">
						<li><a href="https://www.facebook.com/pages/GAP-Metropolitana/1578641942465889" target="_blank"><span class="icon-facebook"></span></a></li>
						<li><a href="#"><span class="icon-twitter"></span></a></li>
						<li><a href="https://www.youtube.com/channel/UCj94Ot96iribtbiSc0LtHqw" target="_blank"><span class="icon-youtube"></span></a></li>
					</ul> -->
				</div>
			</div>
		</div>
		<div class="legal">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col">
						<p>Paseo de la Reforma 403 Col. Cuauhtémoc, Deleg. Cuauhtémoc C.P. 06500 CDMX, Piso 7</p>
						<p><strong>GAP METROPOLITANA</strong> © 2018. Algunos derechos reservados&nbsp;&nbsp; |&nbsp;&nbsp; <a href="<?php echo $path; ?>aviso-de-privacidad" class="aviso">Aviso de Privacidad</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	
	
	<script src="<?php echo $path;?>js/jquery-3.0.0.min.js"></script>
	<script src="<?php echo $path;?>js/bootstrap.min.js"></script>
	<script src="<?php echo $path;?>js/owl.carousel.min.js"></script>
	<script src="<?php echo $path;?>js/prototipos.js"></script>
	<script src="<?php echo $path;?>js/jquery.validate.min.js"></script>
	<script src="<?php echo $path;?>js/main.js"></script>
	<script src="<?php echo $path;?>js/jquery.squares.js"></script>
	<script src="<?php echo $path;?>js/baguetteBox.min.js" async></script>
	
	
	<?php 
	$pos = strpos($page, 'contacto');
	// if($page == '/contacto' || $page == '/'):
	if($pos || $page =='/'): ?>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcb5wynboneWdUK_PHU5heEO0c6wWxkfw&callback=initMap" async defer></script>
	<script>
		/*maps */
		var map;
		function initMap(var_lat=19.457044,var_lgn=-99.123554) {
			var ubication = {lat:var_lat, lng: var_lgn};
			map = new google.maps.Map(document.getElementById('map'), {
				center: ubication,
				zoom: 15
			});
			var marker = new google.maps.Marker({
				position: ubication,
				map: map,
				icon:"images/pin.png"
			});
		}
	</script>
	<?php endif; ?>

	<!-- Google Code para etiquetas de remarketing -->
	<!-- <script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 803713739;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/803713739/?guid=ON&amp;script=0"/>
	</div>
	</noscript> -->
</body>
</html>