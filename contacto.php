<?php
    if($_POST){
        if( empty($_POST["nombre"])             ||
            empty($_POST["telefono"])           ||
            empty($_POST["email"])              ||
            empty($_POST["medio_contacto"])     ||
            empty($_POST["desarrollo_interes"]) ||
            empty($_POST["tipo_credito"])       || 
			empty($_POST["message"])            ||
			empty($_POST["captcha_code"])       ||
            empty($_POST["captcha_value"])
            ){
            exit("Error, información incompleta.");
		}else if ($_POST["captcha_code"] != $_POST["captcha_value"]) {
            exit("Error, el código no es correcto.");
        }else if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            exit("Error, email no válido.");
        }
        else{
            date_default_timezone_set('America/Mexico_City');
            setlocale(LC_TIME, 'es_ES');
            require_once 'PHPMailer/PHPMailerAutoload.php';

            $fullname       =       $_POST["nombre"];
            $telefono       =       $_POST["telefono"];
            $email          =       $_POST["email"];
            $medio_contacto =       $_POST["medio_contacto"];
            $interes        =       $_POST["desarrollo_interes"];
            $credito        =       $_POST["tipo_credito"];
            $message        =       $_POST["message"];
            $date           =     strftime('%e de %B de %Y a las %H:%M horas');

            $html = file_get_contents(dirname(__FILE__)."/form/template-contacto.html");

            $html = str_replace("{%fullname%}"     , $fullname          , $html);
            $html = str_replace("{%phone%}"        , $telefono          , $html);
            $html = str_replace("{%email%}"        , $email             , $html);
            $html = str_replace("{%medio%}"        , $medio_contacto    , $html);
            $html = str_replace("{%interes%}"      , $interes           , $html);
            $html = str_replace("{%credito%}"      , $credito           , $html);
            $html = str_replace("{%message%}"      , $message           , $html);
            $html = str_replace("{%date%}"         , $date              , $html);

            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;
            // $mail->SMTPOptions = array(
            //     'ssl' => array(
            //         'verify_peer' => false,
            //         'verify_peer_name' => false,
            //         'allow_self_signed' => true
            //         )
            //     );
			$mail->SMTPAuth = false;
			$mail->SMTPSecure = 'none';
			$mail->Host = "localhost";
			$mail->Port = 25;
            $mail->IsHTML(true);
            $mail->Username = "postmaster@publicidadenlinea.com";
            $mail->Password = "Pel@14";
            $mail->CharSet = 'UTF-8';
            $mail->From = $email;
            $mail->FromName = $fullname;
            $mail->Subject = "Contacto - Gap Metropolitana";
            $mail->Body = $html;
            $mail->addAddress("info@gapmetropolitana.com.mx", "Información Gap Metropolitana");
            $mail->AddBCC("luis.vargas@publicidadenlinea.com", "Luis Vargas");
            $mail->AddBCC("daniel.rojas@publicidadenlinea.com", "Daniel Rojas");
            $mail->AddBCC("gaby.ramos@publicidadenlinea.com","Gabriela Ramos");

            $mail->addReplyTo($email, $fullname);
            if($mail->Send()){
                header('Location: /gracias'); 
            }else{
                exit("Error, comentario no enviado.");
            }
        }
    }
	$captcha = substr(md5(mt_rand()), 0, 4);
	include_once('header.php');
?>

<section class="header quienes-somos">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1>CONTACTO</h1>
            </div>
        </div>
    </div>
</section>

<section class="contact page-contact">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-5 offset-xl-1 col-lg-12 offset-lg-0">
					<h2>DEJA TU MENSAJE</h2>
					<p>*Por favor llena los campos obligatorios.</p>
					<form id="form-contact" class="form-contact" action="<?php echo $path; ?>contacto" method="POST">
						<div class="form-group">
							<input type="text" class="form-control" id="nombre" name="nombre" placeholder="* Nombre Completo">
                        </div>
                        <div class="form-group">
							<input type="text" class="form-control" id="telefono" name="telefono" placeholder="* Teléfono">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="email" name="email" placeholder="* Correo electrónico">
						</div>
                        <div class="form-group">
							<select name="medio_contacto" id="medio_contacto" class="form-control">
                                <option value="">*Medio de Contacto</option>
								<option value="Redes Sociales">Redes Sociales</option>
								<option value="Lona">Lona</option>
								<option value="Volante">Volante</option>
								<option value="Metro">Metro</option>
								<option value="Gallardete">Gallardete</option>
								<option value="Poster">Poster</option>
								<option value="Google">Google</option>
								<option value="Publicidad Digital">Publicidad Digital</option>
							</select>
                        </div>
                        <div class="form-group">
							<select name="desarrollo_interes" id="desarrollo_interes" class="form-control">
								<option value="">*Desarrollo de Interés</option>
								<option value="Mellado">Mellado</option>
								<option value="Mezquital">Mezquital</option>
								<option value="Arteaga">Arteaga</option>
								<option value="Clavijero">Clavijero</option>
								<option value="Sur 20 162">Sur 20 162</option>
								<option value="Sur 20 168">Sur 20 168</option>
								<option value="Sur 20 174">Sur 20 174</option>
							</select>
                        </div>
                        <div class="form-group">
							<select name="tipo_credito" id="tipo_credito" class="form-control">
								<option value="">* Tipo de crédito</option>
								<option value="INFONAVIT">INFONAVIT</option>
								<option value="FOVISSSTE">FOVISSSTE</option>
								<option value="Contado">Contado</option>
								<option value="Otro">Otro</option>
							</select>
						</div>
						<div class="form-group">
							<textarea class="form-control" id="message" name="message" rows="3" placeholder="* Mensaje"></textarea>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="captcha_value" name="captcha_value" placeholder="* Ingresa el código">
							<p class="completar"><?php echo $captcha;  ?></p>
							<input type="hidden" id="captcha_code" name="captcha_code" value="<?php echo $captcha ?>">
						</div>
						<div class="form-group">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="accept_privacy_policies" name="accept_privacy_policies">
								<label class="form-check-label" for="gridCheck">
									He leído el  <a href="<?php echo $path; ?>aviso-de-privacidad" target="_blank">Aviso de Privacidad.</a>
								</label>
							</div>
						</div>
                        <button type="submit" class="btn btn-primary">ENVIAR</button>
                        
					</form>
                </div>
                <div class="oficinas">
                    <ul>
                        <li>
                            <h3>OFICINAS DE VENTA</h3>
                        </li>
                    </ul>
                </div>
				<div class="col-xl-5 offset-xl-1 col-lg-6 col-md-6 col-sm-12 col-12 order-1">
					<div id="map"></div>
				</div>
			</div>
		</div>
	</section>

<?php include_once('footer.php'); ?>

<script type="text/javascript">
    var $template_oficinas = "<li>"+
									"<a href='#' data-lat='{lat}' data-long='{long}'>"+
										"<h5>{nombre_oficina}</h5>"+
										"<p>{direccion_oficina}</p>"+
									"</a>"+
                                "</li>";
    

    $.each(oficinas,function(index,value){
			$(".oficinas ul").append(
				$template_oficinas.replace("{nombre_oficina}",value.nombre).replace("{direccion_oficina}",value.direccion).replace("{lat}",value.lat).replace("{long}",value.long).replace(/<br>([^_]*)$/,'$1',"")
			);
        });
        
    jQuery(document).ready(function($){
        var parametro = window.location.search;
        if(parametro){
            console.log("viene parametro");
            var param = window.location.search.split("=")[1];
            var val = param.replace(/-/g," ");
            console.log(val);
            $("#desarrollo_interes option[value='"+val+"']").attr("selected","selected");
        }
    });

</script>