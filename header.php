<?php
$path = "http://gap.pel:8080/";
//$path = "http://gapmetropolitana.com.mx/";
$page = $_SERVER['REQUEST_URI'];

if(!isset($title) || !isset($description) || !isset($keywords)){
	$title = 'Gap Metropolitana';
	$description = 'Empresa inmobiliaria con 30 años de experiencia y 30,000 viviendas contruídas dentro del sector de vivienda económica.';
	$keywords = 'inmobiliaria, inmobiliarias df, empresas inmobiliarias, vivienda económica, desarrollos inmobiliarios, departamentos en venta';
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no,user-scalable=no">
	<meta name="description" content="<?php echo $description; ?>">
	<meta name="keywords" content="<?php echo $keywords; ?>">
	<meta name="google-site-verification" content="-rNKAWKzWpwWJ7Rd1k8g1Nztw2l6EffGf-ENpIHrHig" />
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo $path;?>css/normalize.min.css">
	<link rel="stylesheet" href="<?php echo $path;?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $path;?>css/animate.css">	
	<link rel="stylesheet" href="<?php echo $path;?>css/owl.carousel.min.css">
	<!-- <link rel="stylesheet" href="<?php echo $path;?>css/main.css"> -->
	<link rel="stylesheet" href="<?php echo $path;?>css/style.css">
	<link rel="stylesheet" href="<?php echo $path;?>css/baguetteBox.min.css">
	<link rel="icon" type="image/png" href="<?php echo $path;?>images/favicon.png">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119763792-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	
	gtag('config', 'UA-119763792-1');
	</script> -->
	<!-- Facebook Pixel Code -->
	<!-- <script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '262095087866657');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=262095087866657&ev=PageView&noscript=1"
	/></noscript>
	End Facebook Pixel Code -->
</head>
<body>
	<div class="preloader"></div>
	
	<nav class="navbar navbar-top">
		<div class="container">
			<div class="w-100">
			<span class="icon icon-whatsapp"></span> <a href="https://api.whatsapp.com/send?phone=5215583217235" target="_blank">521-558-321-7235</a><span class="separator"></span><span class="icon icon-phone"></span> <a href="tel:41-61-30-03">41-61-30-03</a> <span class="separator"></span> <span class="icon icon-building"></span> <a href="<?php echo $path; ?>#contact" onclick="goTo();">Oficinas</a>
			</div>
		</div>
	</nav>
	<nav class="navbar main navbar-expand-lg navbar-light">
		<div class="container">
			<a class="navbar-brand" href="<?php echo $path; ?>">
				<img src="<?php echo $path; ?>images/gap-metropolitana-logo-alt.png" class="img-fluid" alt="GAP METROPOLITANA">
				<div class="division"></div>
				<img src="<?php echo $path; ?>images/tu-espacio-en-la-ciudad.png" class="img-responsive" alt="Tu espacio, tu ciudad">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="mainMenu">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item <?php if(strpos($page,'departamentos-en-venta')){ echo "active";} ?>">
						<a class="nav-link" href="<?php echo $path; ?>#developments">DESARROLLOS</a>
					</li>
					<li class="nav-item <?php if($page == "/nuestra-inmobiliaria"){ echo "active";} ?>">
						<a class="nav-link" href="<?php echo $path; ?>nuestra-inmobiliaria">GAP METROPOLITANA</a>
					</li>
					<li class="nav-item">
						<!-- <a class="nav-link" href="<?php echo $path; ?>#testimoniales">TESTIMONIOS</a> -->
					</li>
					<li class="nav-item <?php if($page == "/contacto"){ echo "active";} ?>">
						<a class="nav-link" href="<?php echo $path; ?>contacto">CONTACTO</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="btn-contacto">
		<a href="<?php echo $path; ?>contacto"><span class='icon-mail'></span><p>CONTACTO<p></a>
	</div>
