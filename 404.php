<?php include_once('header.php'); ?>

<section class="header quienes-somos">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1>GAP METROPOLITANA</h1>
            </div>
        </div>
    </div>
</section>

<section class="aviso-privacidad">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h2 style="font-size:140px">404</h2>
                <p style="text-align:center">La página no esta disponible.</p>
            </div>
        </div>
    </div>
</section>

<?php include_once('footer.php'); ?>